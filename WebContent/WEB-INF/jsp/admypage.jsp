<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<div class="content">
	<style>
#list {
	color: black;
	border: solid 1px;
	text-align: center;
	width: 800px;
	/* 	margin-left: 350px; */
	margin: auto;
}

#store_list {
	/* 	border: 10px solid transparent; */
	text-align: center;
	table-layout: fixed;
	/* border: 1px solid; */
}

#td_menu {
	vertical-align: bottom;
}

#menu_modal {
	/* width: auto; */
	height: 100%;
	width: 100%;
}

.td_menu {
	width: auto;
}
</style>

	<h2 class="title">메인페이지</h2>
	<div class="span9" id="content">
		<div class="row-fluid">

			<div class="alert alert-warning">
				<h2 class="sect_tit" align="senter">${sessionScope.stname }님의
					마이페이지</h2>

				<p>이름 : ${sessionScope.stname}</p>

				<p>주소 : ${svo.staddr1}${svo.staddr2}</p>

				</h2>
			</div>
			<!--정보 수정  -->
			<ul class="row hidden-sm clients mt2">
				<li class="banner-wrap col-sm-3 brd-btm">
					<figure class="featured-thumbnail">
						<a href="stmyupdate" data-toggle="tooltip" title="회원 정보 수정"> <img
							alt="" src="resources/img/logos/my.png" title="회원 정보 수정">
					</figure>
				</li>

				<!--메뉴리스트/수정/삭제    -->

				<li class="banner-wrap col-sm-3 brd-btm">
					<figure class="featured-thumbnail">
						<a id="menu_modal" data-target="#upform" data-toggle="modal">
							<p data-toggle="tooltip" title="메뉴리스트/수정/삭제">
								<img alt="" src="resources/img/logos/list.png" title=""
									class="img-responsive">
						</a>
					</figure>
				</li>

				<!-- <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
           <a id="menu_modal" data-target="#upform" data-toggle="modal"> 
            <p data-toggle="tooltip" title="메뉴리스트/수정/삭제"> 
         <img alt=""   src="resources/img/logos/list.png" title="" class="img-responsive">
         </a>
         </figure>
     </li> -->

				<!--주문내용  -->
				<li class="banner-wrap col-sm-3 brd-btm">
					<figure class="featured-thumbnail">
						<a id="menu_modal" data-target="#order_history"
							data-toggle="modal">
							<p data-toggle="tooltip" title="주문 내용 확인 ">
								<img alt="" src="resources/img/logos/order.png" title=""
									class="img-responsive">
						</a>
					</figure>
				</li>

				<!--로그 보기  -->
				<li class="banner-wrap col-sm-3 brd-btm">
					<figure class="featured-thumbnail">
						<a data-toggle="tooltip" title="매뉴 리스트 " onclick="location='stmypage'"> <img
							alt="" src="resources/img/logos/de.png" title=""
							class="img-responsive" >
						</a>
					</figure>
				</li>
			</ul>
			<ul class="row hidden-sm clients mt2">
				<!--홈  -->
				<li class="banner-wrap col-sm-3 brd-btm">
					<figure class="featured-thumbnail">
						<a href="#" onClick="home()" data-toggle="tooltip" title="메인">
							<img alt="" src="resources/img/logos/main.png" title=""
							class="img-responsive">
						</a>
					</figure>
				</li>

			</ul>
		</div>
	</div>



	<div class="modal fade" id="upform">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- header -->
				<div class="modal-header">
					<!-- 닫기(x) 버튼 -->
					메뉴 등록 폼
					<button type="button" class="close" data-dismiss="modal">×</button>
					<!-- header title -->
				</div>
				<!-- body -->
				<div class="modal-body">
					<form method="post" action="upsave" autocomplete="off"
						enctype="multipart/form-data">
						<input type="hidden" name="stnum" id="stnum"
							value="${sessionScope.stnum}">
						<p>
							<label>음식이름</label> <input type="text" name="foodname"
								id="foodname" placeholder="음식 명을 입력하세요" required="required">
						</p>
						<p>
							<label>음식 가격</label> <input type="text" name="foodpay"
								id="foodpay" placeholder="음식 값을 적어주세요" required="required">
						</p>
						<p>
							<label>음식 사진</label> <input type="file" name="upimg" id="upimg">
						</p>
						<p>
							<label>음식 상세 정보</label>
						</p>
						<p>
							<textarea name="fooddetail" id="fooddetail" cols="40" rows="8"></textarea>
						</p>
						<p>
							<label>음식 종류</label><select name="category" id="category">
								<option value="마카롱">마카롱</option>
								<option value="마카롱세트">마카롱세트</option>
								<option value="음료">음료</option>
								<option value="HOT">커피 HOT</option>
								<option value="ICE">커피 ICE</option>
								<option value="기타">기타</option>
							</select>
						</p>
						<p style="text-align: right;">
							<input type="submit" value="등록"
							class="btn btn-success"
							><input type="button"
								value="리스트" onclick="location='stmypage'"
									class="btn btn-info"
								>
						</p>
					</form>
				</div>
				<!-- Footer -->
				<div class="modal-footer">
		
					<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 주문 내역 확인 -->

<div class="modal fade" id="order_history">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- header -->
			<div class="modal-header">
				<div class="modal-header">
					<p>${sessionScope.adminid}님의
						판매 정보
						<button type="button" class="close" data-dismiss="modal">×</button>
					</p>
				</div>
				<!--   memid / pay_time / pay_total / foodname / dok / foodtotal /  -->
				<div class="modal-body">
					<table style="width: 100%">
						<tr>
							<td class="td_menu">구매 고객 ID</td>
							<td class="td_menu">주문 음식</td>
							<td class="td_menu">주문 수량</td>
							<td class="td_menu">주문 금액</td>
							<td class="td_menu">배달 유무</td>
							<td class="td_menu">구매 시간</td>
						</tr>
						<c:forEach var="ex" items="${paylist}">

							<tr>
								<td>${ex.membervo.memid}</td>
								<td>${ex.foodname}</td>
								<td>${ex.foodtotal}</td>
								<td>${ex.pay_total}</td>
								<td><c:choose>
										<c:when test="${ex.dok eq 0}">배달</c:when>
										<c:when test="${ex.dok eq 1}">직접 수령</c:when>
									</c:choose></td>
								<td>${ex.pay_time}</td>

							</tr>
						</c:forEach>
					</table>
				</div>
				<!-- Footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
</div>






<script>
	// 숫자가 아닌 정규식
	var replaceNotInt = /[^0-9]/gi;

	$(document).ready(function() {

		$("#foodpay").on("focusout", function() {
			var x = $(this).val();
			if (x.length > 0) {
				if (x.match(replaceNotInt)) {
					x = x.replace(replaceNotInt, "");
				}
				$(this).val(x);
			}
		}).on("keyup", function() {
			$(this).val($(this).val().replace(replaceNotInt, ""));
		});

	});
</script>


<%@include file="footer.jsp"%>