<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
</script>

<br>
<br>
<br>
<br>
<br>
<br>
<div class="content">

<style>
#list {
	color: black;
	border: solid 1px;
	text-align: center;
	width: 1000px;
	margin: auto;
}

#store_list th {
	text-align: center;
}
</style>

<section id="cart-view">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="cart-view-area">
<div class="cart-view-table">

<div class="table-responsive">
			<table class="table" id="basket_list">
			<h1 style="text-align: right;">${sessionScope.uname }님의 장바구니</h1> <!-- 세션에 저장되어있는 uname을 사용하도록 가져오는 방법 -->
				<thead>
				<tr>
				<!-- "전체선택" 버튼 만들고, 클릭시(onclick) 괄호안 번호에 따라 전체 선택인지 선택 취소인지 나뉜다 -->
				<!-- 괄호안 값이 1번일 경우에는 아래에  "function checkboxSelectQue(n,obj)" 에서 정의된 그대로 기능을 한다 -->
				<!-- chk[]의 경우 chk[]이라고 name 붙여진 항목들이 "전체선택" 버튼이 클릭되면(onclick) 선택되도록 설정된 것 -->
				<th style="text-align: center;"><input type="button" value="전체선택" onclick="checkboxSelectQue(1,'chk[]')"/>
				</th>
				</thead>
				<tbody>
					<c:forEach var="dto" items="${dtolist }"> <!-- controller 에서 받아오는 dtolist의 값들을 var="dto"로 받는다 -->
					<form method="post">
						<tr>
							<td style="text-align: center">
							<input class="chBox" type="checkbox" name="chk[]" data-cnum="${dto.basketnum}"></td> 
							<!-- 체크박스 설정: 클래스와 name을 설정해서 아래의 "function checkboxSelectQue(n,obj)" 함수에서 기능 구현할 수 있도록 함 -->
							<!-- data-cnum 은 장바구니 개별 목록에 해당하는 번호, 이것도 동일 함수 안에서 사용된다 -->
							<%--<td><img src="resources/imgfile/${dto.foodimg}" style="width: 25px;"></td> <!-- 메뉴 이미지 -->--%>
							<td>${dto.foodname}</td> <!-- 상품 이름 보여주기 -->
							<td> 가격 : ${dto.foodpay }</td>
							<td>수량 : <label>${dto.foodtotal }</label></td>
							<td>총가격 : ${dto.basketpay } 원</td>
							<td> 수령 방법 : 
							<c:choose>
							<c:when test="${dto.dok  eq 0}">배달</c:when>
							<c:when test="${dto.dok  eq 1}">TakeOut</c:when>
							</c:choose>
							<td><a href="deleteselected?basketnum=${dto.basketnum }">삭제</a></td> <!-- 개별 삭제 -->
						</tr>
						</form>
					</c:forEach>
				</tbody>
				<tfoot>
				<tr>
				<!-- "function checkboxSelectQue(n,obj)" 함수에 의해 작동 -->
				<!-- 괄호 안 값에 2를 부여하여 위의 함수에서 버튼 2일때 해당 기능이 작동하도록 함수 안에 설정 -->
				<!-- 1번 전체선택 버튼과 동일하게 name 이 chk[]인 체크박스들에게 적용되도록 설정 -->
				<th style="text-align: center;"><input type="button" value="선택취소" onclick="checkboxSelectQue(2,'chk[]')"/></th>
				</tr>
				<tr>
				<!-- 또 다른 함수인  " $('#purchaseBtn').click(function () {" 에 의해 작동되는 "구매" 버튼 -->
				<!-- 기능은 아래의  " $('#purchaseBtn').click(function () {" 에서 설명 -->
				<th style="text-align: right;"><button type="button" id="purchaseBtn" name="purchaseBtn"
                                        class="btn btn-main pull-right" onclick="">구매</button></th>
				</tr>
				</tfoot>
			</table>
			<form action="clickPay" method="post"> <!-- 컨트롤러에 있는 맵핑 값이 clickPay인 곳으로 전달  -->
								<c:forEach var="clickPay" items="${dtolist }">
									<tr><!-- 전달할 값들을 입력 받음 -->
										<td><input type="hidden" id="memnum" name="memnum" value="${clickPay.memnum }">
										<td><input type="hidden" id="stnum" name="stnum" value="${clickPay.stnum }"></td>
										<td><input type="hidden" id="pay_total" name="pay_total" value="${clickPay.basketpay }" ></td>
										<td><input type="hidden" id="foodname" name="foodname" value="${clickPay.foodname }" ></td>
										<td><input type="hidden" id="dok" name="dok" value="${clickPay.dok }" ></td>
										<td><input type="hidden" id="category" name="category" value="${clickPay.category }" ></td>
										</tr>
									</c:forEach>
									<div class="cart-view-total" style="text-align: right;">
								<h4>
<!-- 									"=" 을 클릭하면 장바구니 내의 총 구매 금액이 합쳐지고 표시된다 -->
									총 결제 금액 =
									<%--<input type="button" value="=" onclick="location.href='listBasket2?memnum=${sessionScope.unum}'"> --%>
<!-- 									총 결제 금액을 1,000,000,000 형식으로 표시한다 -->
									<fmt:formatNumber value="${tp.totalprice }" pattern="###,###,###" />
									원
								</h4>
<!-- 								결제할 것인가 장바구니에 다른 상품을 다 담을 것인가를 고를 수 있게 하이퍼텍스트를 주었다 -->
								<button type="button" onclick="location.href='storelistController?page=1'" >상품 더 담으러 가기</button>
							</div>
							</form>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			</section>
	<br> <br> <br>
	
	<script>
	function checkboxSelectQue(n,obj){ // 체크박스 선택할 때 작동하는 function
		var i; 
		var chk = document.getElementsByName(obj); 
		for (i = 0; i<chk.length; i++){
			if(n == 1) chk[i].checked = true; // 전체선택
			if(n == 2) chk[i].checked = false; // 전체해제
		}
	}
	
	function checkboxSelectCount(obj){ // 체크박스가 몇개나 선택 되었는가에 대한 function
		var i, sum=0, tag=[], str="";
		var chk = document.getElementsByName(obj);
		for (i=0; i<chk.length; i++){
			if (chk[i].checked == true){ // 체크되었다면
				tag[sum] = chk[i].value; //
				sum++;
			}
		}
		str +="선택 갯수 : "+sum;
		if (tag.length > 0) str +="\n값 : "+tag.join(",");
		alert(str);
	}
</script>

<script>
   $('#purchaseBtn').click(function () { // id가 purchaseBtn 인 버튼을 감지하는 함수(function)
      var checkArr = new Array(); // 배열로 checkArr를 생성
        if(confirm("정말 구매 하시겠습니까?") == true) { // 구매 버튼 클릭 후 알림창의 확인버튼 누르면 아래로 내려가 차례로 작동
           var $form = $('<form></form>');
           $form.attr('action','clickPay'); // key와 value 순으로 action이란 key에 대한 value가 clickPay
           $form.attr('method','post'); // 위와 동일하게 method라는 key에 대한 value가 post ; 즉 <form></form> 안에 있는 내용을 post 방식으로 보냄
           $form.appendTo('body');
           
           $("input[class='chBox']:checked").each(function() { // chBox라고 class 값이 정해진 체크박스들 중 체크상태인 것들에 대한 함수 만듬 
               checkArr.push($(this).attr("data-cnum")); // 위에서 봤던 data-cnum="${dto.basketnum}", 장바구니 항목 번호를 checkArr라는 배열에 하나씩 저장
               // 즉, 체크박스에서 체크된 항목의 장바구니 항목 번호를 배열에 저장
         });
           // chk라는 변수 생성해서 input 문 작성하고 위의 배열을 value로 받음
           var chk = $("<input type='hidden' value='"+checkArr+"' name='chk[]'>");    
           $form.append(chk);
          
          if(checkArr.length){ // checkArr라는 배열의 길이를 감지
           $form.submit(); // 체크된 항목이 있으면 <form></form> 형태로 제출
          }else{
            alert("체크해 주세요"); // 체크된 항목이 없으면 메시지 보여줌
          }
        }  
   })
   
</script>
<!-- 아래는 상품을 장바구니에 담은 직후 띄워주는 메시지이다. -->
	<script type="text/javascript">
		$(document).ready(function gobackCheck() {
			if (confirm("[취소]를 누르면 가게 리스트로 이동할 수 있습니다.") == true) {
			} else { // [확인]을 누르면 장바구니 페이지에 머물고 [취소]를 누르면 가게 리스트 페이지로 넘어갈 수 있음
				location.href="storelistController?page=1"; 
			}
		});
	</script>

</div>
<%@include file="footer.jsp"%>