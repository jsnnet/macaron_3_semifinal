<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>

<br>
<br>
<br>
<br>
<br>
<br>
<style>
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
#list {
   color: black;
   border: solid 1px;
   text-align: center;
   width: 300px;
/*    margin-left: 350px; */
   margin: auto;
}
</style>
<div id="list">
<!-- <div style="margin: auto;text-align: center;"> -->
 <fieldset style="border: 1px solid rgba(255, 255, 255, .5);" id="container">
<legend>${sessionScope.uname}님의 친구리스트 입니다</legend>
<label> 더치페이 할 친구를 선택 하세요</label>
<table  style="border: 1px solid rgba(255, 255, 255, .5);"  id="container">

	<c:forEach var="e" items="${friend_list}">
			<td><input type="checkbox" name="friendid" id="${e}"
				value="${e}" onclick="modal_btn(${e})">${e}</td>
	</c:forEach>
</table>
<form method="post" action="dutch_pro" autocomplete="off"
	enctype="multipart/form-data" >
	<input type="hidden" name="stnum" value="${pvo.stnum}">
	<input type="hidden" name="pay_total" value="${pvo.pay_total}">
	<input type="hidden" name="foodname" value="${pvo.foodname}">
	<input type="hidden" name="foodpay" value="${pvo.foodpay}">
	<input type="hidden" name="foodtotal" value="${pvo.foodtotal}">
	<input type="hidden" name="dok" value="${pvo.dok}">
	<input type="hidden" name="category" value="${pvo.category}">
	<label style="font: oblique;">지불 금액 : ${pvo.pay_total}</label> <br/>
	<input type="text" id="target1" name="memid2" readonly="readonly" placeholder="친구 1" style="width: 40%">
	<input type="text" id="target2" name="memid3" readonly="readonly" placeholder="친구 2" style="width: 40%"><br>
	 <input type="number" id="pay1" name="pay" placeholder="지불 할 금액" max="${pvo.pay_total}" min="0" min="${pvo.pay_total}" value="0" style="width: 40%">
	<input type="submit" value="제출">
	
</form>
    </fieldset>
   </div>
<!-- </div> -->
<script>
	let arrNumber = new Array();
	var $modalnum;

    function aa (){
       $modalnum.on('click',function(){
         
      	
        
      })
    }
function modal_btn(ele){
 
   let divname = $(ele).attr('id');
   $modalnum = $('#' + divname);
   
   
   let chkCnt = $('input:checkbox[name="friendid"]:checked').length;
   
	if(chkCnt>2){
		alert("2명까지 선택 가능 합니다.");
		ele.checked = false;
		return false;
	}else{
		check($modalnum);
	}
}
	  function check($modalnum){
		let value1 = $modalnum.attr('id')
		if($modalnum.is(":checked")){
            if($('#target1').attr('value') == null){
    			$('#target1').attr('value',value1);
    		}else if($('#target1').attr('value') !== null && $('#target2').attr('value') == null){
    			$('#target2').attr('value',value1);
    		}
        }else{
            if($('#target1').attr('value') == $modalnum.val()){
    			$('#target1').attr('value',null);
    		}else if($('#target2').attr('value') == $modalnum.val()){
    			$('#target2').attr('value',null);
    		}
        }
	}; 
	  
	//번호만 주는 쿼리문
	var replaceNotInt = /[^0-9]/gi;

	$(document).ready(function() {

		$("#pay1").on("focusout", function() {
			var x = $(this).val();
			if (x.length > 0) {
				if (x.match(replaceNotInt)) {
					x = x.replace(replaceNotInt, "");
				}
				$(this).val(x);
			}
		}).on("keyup", function() {
			$(this).val($(this).val().replace(replaceNotInt, ""));
		});

	});

</script>
<%@include file="footer.jsp"%>