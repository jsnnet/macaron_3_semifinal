<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="header.jsp"%>
<style>
#syl {
	width: 100%;
	margin: auto;
	text-align: center;
}
#list {
   color: black;
   border: solid 1px;
   text-align: center;
   width: 1500px;
/*    margin-left: 350px; */
   margin: auto;
}
</style>
<style>
input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button
	{
	-webkit-appearance: none;
	margin: 0;
}
</style>
<br>
<br>
<br>
<br>
<br>
<br>
 <div id="list">
   
      <fieldset style="border: 1px solid rgba(255, 255, 255, .5);" id="container">
<table id="syl">
	<tr>
		<td>결제 음식</td>
		<td>음식 수량</td>
		<td>1번 아이디</td>
		<td>2번 아이디</td>
		<td>3번 아이디</td>
		<td>1번 결제 금액</td>
		<td>2번 결제 금액</td>
		<td>3번 결제 금액</td>
		<td>총 금액</td>
		<td>남은 금액</td>
		<td>결제 수정</td>
		<td>결제 취소</td>
		
	</tr>
	<c:forEach var="e" items="${dutchlist }">
		<tr>
			<td>${e.pvo.foodname}</td>
			<td>${e.pvo.foodtotal}개</td>
			<td>${e.memid1}님</td>
			<td>${e.memid2}님</td>
			<td>${e.memid3}님</td>
			<td>${e.pay1}원</td>
			<td>${e.pay2}원</td>
			<td>${e.pay3}원</td>
			<td>${e.pay_total}원</td>
			<td>${e.maxpay}원</td>
			<c:choose>
				<c:when test="${e.maxpay eq 0}">
				
					<td><button onclick="pay(${e.pay_num})"
					class="btn btn-warning">최종 결제</button></td>
					
				</c:when>
				<c:when test="${e.maxpay ne 0}">
					<td><button id="menu_modal" class="btn btn-default"
							data-target="#layerpop_friendplus${e.pay_num}"
							data-toggle="modal">추가 결제</button></td>
					
				</c:when>
			</c:choose>
			<td><button onclick="cancel(${e.pay_num})"
					class="btn btn-danger">결제 취소</button></td>
			
			<!-- dgalsjhndl;ajhfsdhasdkljghdfwakjgahskjgnakl;jhgvlk;jasdhglahwsdgn,cxvhow -->
			
			<div class="modal fade" id="layerpop_friendplus${e.pay_num}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
					<form action="dutch_update" method="post">
						<input type="hidden" name="pay_num" value="${e.pay_num}">
						<input type="hidden" name="memid1" value="${e.memid1}">
						<input type="hidden" name="memid2" value="${e.memid2}">
						<input type="hidden" name="memid3" value="${e.memid3}">
						
						<input type="number" id ="pay" name="pay" placeholder="값을 입력하세요" min="0" max="${e.pay_total}">	
						<input type="submit" value="수정">
					</form>		
			</div>
			<div class="modal-footer">
				
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>
			
			
		</tr>
	</c:forEach>
	<tr>
		<td colspan="11"><input type="submit" value="집으로 돌아가기"
			onclick="location='myPage'" class="btn btn-primary"></td>
	</tr>
</table>
</fieldset>
</div>
<script>
var replaceNotInt = /[^0-9]/gi;

$(document).ready(function() {

	$("#pay").on("focusout", function() {
		var x = $(this).val();
		if (x.length > 0) {
			if (x.match(replaceNotInt)) {
				x = x.replace(replaceNotInt, "");
			}
			$(this).val(x);
		}
	}).on("keyup", function() {
		$(this).val($(this).val().replace(replaceNotInt, ""));
	});

});

function cancel(pay_num) {

	$.ajax({
		url: './store/dutch_delete',
		type: 'post', 
		data: { pay_num:pay_num },
		success: function(data){
			alert(data);
			location="dutch_status";
		}
	});

}

function pay(pay_num) {

	$.ajax({
		url: './store/pay',
		type: 'post', 
		data: { pay_num:pay_num },
		success: function(data){
			
			alert(data);
			location="dutch_status";
		}
	});

}
</script>



<%@include file="footer.jsp"%>