<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp" %>
<script>
var i = 1;
$(document).ready(function(){
  photo();
});

playAlert = setInterval(function() {
     $('#photostack-1 span:nth-child('+i+')').attr('class', 'current').trigger('click');
     i++;
     if(i == 7){
        i = 1;
     }
   }, 5000);

 function photo(){
   $('#photostack-1').trigger('click');
}

 
 
 function popdetail(stnum){
	 var check = confirm('가게 페이지로 이동하시겠습니까?');
	   if (check) {
	     	location.href = 'storeDetail?stnum='+stnum;
	   }
 }
</script>
  <section id="photostack-1" class="photostack photostack-start">
    <div>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m1.jpg" alt="img01"/></a>
        <figcaption>
          <h2 class="photostack-title">Serenity Beach</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m2.jpg" alt="img02"/></a>
        <figcaption>
          <h2 class="photostack-title">Happy Days</h2>
        </figcaption>WE ARE POLAROYD
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m3.jpg" alt="img03"/></a>
        <figcaption>
          <h2 class="photostack-title">Beautywood</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m4.jpg" alt="img04"/></a>
        <figcaption>
          <h2 class="photostack-title">Heaven of time</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m5.jpg" alt="img05"/></a>
        <figcaption>
          <h2 class="photostack-title">Speed Racer</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m6.jpg" alt="img06"/></a>
        <figcaption>
          <h2 class="photostack-title">Forever this</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m7.jpg" alt="img07"/></a>
        <figcaption>
          <h2 class="photostack-title">Lovely Green</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m8.jpg" alt="img08"/></a>
        <figcaption>
          <h2 class="photostack-title">Wonderful</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m9.jpg" alt="img09"/></a>
        <figcaption>
          <h2 class="photostack-title">Love Addict</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m10.jpg" alt="img10"/></a>
        <figcaption>
          <h2 class="photostack-title">Friendship</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m11.jpg" alt="img11"/></a>
        <figcaption>
          <h2 class="photostack-title">White Nights</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m12.jpg" alt="img12"/></a>
        <figcaption>
          <h2 class="photostack-title">Serendipity</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m13.jpg" alt="img13"/></a>
        <figcaption>
          <h2 class="photostack-title">Pure Soul</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m14.jpg" alt="img14"/></a>
        <figcaption>
          <h2 class="photostack-title">Winds of Peace</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m15.jpg" alt="img15"/></a>
        <figcaption>
          <h2 class="photostack-title">Shades of blue</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src="resources/img/macaron/m16.jpg" alt="img16"/></a>
        <figcaption>
          <h2 class="photostack-title">Lightness</h2>
        </figcaption>
      </figure>
    </div>
  </section>

  <div class="container">
    <div class="row mt presentation">
      <img class="camera aligncenter" src="resources/img/camera.png" alt="">
      <h1 class="centered">마카롱 프로젝트</h1>
      <hr>

    </div>
    <!-- /row -->
<script>
	window.onload = function(){
		$.ajax({
			url:"./store/popular",
			dataType : 'json',
			cache : false,
			success:function(data){
				let tag3 = '';
				let path = '${pageContext.request.contextPath }';
				var stsbname;
				var stimg;
				var index;
				$.each(data,function(idx, key){
					index = (idx+1);
					stimg = key.stimg;
					stsbname = key.stsbname;
					$.each(key.boardvo,function(idx, key){
						tag3 += '<div class="col-md-4 services-bordered">'
						tag3 += '<h4>'+index+'. '+stsbname+'</h4>';
						tag3 += '<a href="storeDetail?stnum=' + key.stnum + '">';
						tag3 += '<a href="javascript:popdetail('+key.stnum+');">';
						tag3 += "<img src="+path+"/resources/imgfile/"+stimg+".jpg";
						tag3 += '></a>';
						tag3 += '<p>평점 평균 : '+key.gradeavg+'</p>';
						tag3 += '</div></div>';
					})
				})
				$('#pop').html(tag3);
			}
		})
	}
</script>
    <div class="row mt2" >
    <h1 class="centered">인기 많은 가게</h1>
      <div class="col-md-12 centered" id ="pop">
      </div>
    </div>
  </div>
  
  

  <ul class="row hidden-sm clients mt2">
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="가게 로고or 사진 넣자 ">
                    <img alt="" src="resources/img/logos/logo-1.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Second Logo">
                    <img alt="" src="resources/img/logos/logo-2.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Third Logo">
                    <img alt="" src="resources/img/logos/logo-3.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Fourth Logo">
                    <img alt="" src="resources/img/logos/logo-4.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Fifth Logo">
                    <img alt="" src="resources/img/logos/logo-5.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Sixth Logo">
                    <img alt="" src="resources/img/logos/logo-6.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Seventh Logo">
                    <img alt="" src="resources/img/logos/logo-7.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Eighth Logo">
                    <img alt="" src="resources/img/logos/logo-8.png" title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
  </ul>

 <%@include file="footer.jsp" %>