<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%-- loginform.jsp --%>
<%@include file="header.jsp"%>
<script>

$(document).ready(function(){
	var errorChk = document.getElementById('error')
	if(errorChk == 'error'){
		alert("로그인을 하셔야 합니다. ");
	}
})

</script>

	<fieldset style="width: 300px; margin: auto; color: black;">
		<div class="container">
			<div class="row">
				<div class="page-header"></div>
				<div class="col-md-3">
					<div class="login-box well">
						<form method="post" action="loginProcess_user" autocomplete="off">
						<input type="hidden" name="method" id="method" value="${method }">
						<input type="hidden" name="stnum" id="stnum" value="${stnum }">							
							<legend>로그인 ${error }</legend>
							<p>
								<label> 아이디</label> <input type="text" name="memid"  id="memid"
									placeholder=" Username" />
							</p>
							<p>
								<label for="mempwd">비밀번호</label> <input type="password"
									name="mempwd" id="mempwd" placeholder="Password" />
							</p>
							<div class="form-group">
								<input type="submit"
									class="btn btn-default btn-login-submit btn-block m-t-md"
									value="Login" />
							</div>
						
							<hr />
							<div class="form-group">
								<a href="member" class="btn btn-default btn-block m-t-md">
									회원가입</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</fieldset>

<%@include file="footer.jsp"%>
