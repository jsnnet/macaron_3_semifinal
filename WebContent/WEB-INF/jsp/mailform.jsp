<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Send an e-mail</title>
<style>
#login {
	width: 70%;
	margin: auto;
}
</style>
<style>
	#form {
    /* Just to center the form on the page */
    margin: 0 auto;
    width: 400px;
    /* To see the outline of the form */
    padding: 1em;
    border: 1px solid #CCC;
    border-radius: 1em;
}
</style>

<!-- Bootstrap -->
<link href="../plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- font awesome -->
<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">
<!-- Custom style -->
<link rel="stylesheet" href="../plugin/bootstrap/css/style.css"
	media="screen" title="no title" charset="utf-8">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->



<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="../config/js/join.js"></script>
</head>
<body>
<div id="form">
		<article class="container">
			<div class="page-header"></div>
	<form action="send" method="post" enctype="multipart/form-data">
		<table border="0" width="60%" align="center">
			<caption>
				<h2>Send New Q&A E-mail</h2>
			</caption>
			<tr>
				<input type="hidden" name="receiver" value="okstu11@naver.com" />
				<td>이름 : <input type="text" name="name" size="25"
					placeholder="이름을 작성해 주세요."><br>
			<tr>
				<td>제목 : <input type="text" name="subject" size="30" /></td>
			</tr>
			<tr>
				<td><textarea rows="10" cols="39" name="content" placeholder="내용을입력해주세요"></textarea></td>
			</tr>
			<tr>
			<div class="form-group text-center">
				<td colspan="2" align="left">
				<button type="submit" class="btn btn-primary" value="메일보내기">
					메일보내기<i class="fa fa-times spaceLeft"></i>
				</button>
				</td>
			</div>
			</tr>
		</table>
	</form>
</body>
</html>

<%@include file="footer.jsp"%>


