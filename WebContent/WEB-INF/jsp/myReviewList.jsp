
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="header.jsp" %>

<style>
#content th,td,tr {
	color: #000;
	padding: 10px;
}

#content a:link {
	color: #000000;
	text-decoration: none;
}

</style>

<br>
<br>
<br>
<br>
<br>

<div id="article">
<fieldset style="text-align: center">
<legend>${sessionScope.uname}[${sessionScope.uid}]님 후기 게시판</legend>
	<tbody>
		<table style="margin-left: auto; margin-right: auto; " >
	        <tr>
				<td>번호</td>
				<td>사진</td>
				<td>제목</td>
				<td>작성 날짜</td>
				<td>평점</td>
			</tr>
		   
				<c:forEach var="e" items="${list }">
					<tr>
						<td>${e.postsnum }</td>
						<td>
							<img src="./resources/imgfile/${e.img}" style="width: 100px">
						</td>
						<td><a href="tboardDetail?postsnum=${e.postsnum}">${e.title}</a></td>
						<td>${e.pdate }</td>
						<td>${e.grade }</td>
					</tr>
				</c:forEach>
				
				<tr>
				<p>
					<th colspan="9" style="text-align: right;">
						<input type="button" value="글쓰기" id="wBtn">
					</th>
					
				</tr>
		</table>
		</tbody>
	</fieldset>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(function(){
		$('#wBtn').click(function(){
			location="orderList";
		});
	});
</script>

<tfoot>
<table style="margin-left: auto; margin-right: auto; " >
	<tr>
	<tr>
		<th colspan="6">
		<%@include file="page3.jsp"%>
		</th>
	</tr>
	</table>
	<br>
	<table style="margin-left: auto; margin-right: auto; " >
	<th colspan="6">
		<form action="boardUplist" method="post">
			<input type="hidden" name="page" value="${param.page}">
			 <select
				name="searchType">
				<option value="4">내용</option>
				<option value="2">평점</option>
				<option value="3">제목</option>
			</select>&nbsp; <input name="searchValue"> 
			<input type="submit" value="검색">
		</form>
	</th>
	</table>
	</tr>

</tfoot>

