<%@ page language="java" contentType="text/html; charset=EUC-KR"
   pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>

<h2 class="title">메인페이지</h2>
<div class="content">

  <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
                                  <h4>${sessionScope.uname }님의 마이페이지</h4>

                     </div>
                      <form method="post" action="mylog" name="frm">
                     <div class="table-responsive">
                     <table class="table">
                       <tr class="success">
                         <th>회원접속로그조회</th>
                         <th></th>
                         <th></th>
                         <th></th>
                       </tr>
                       
      <c:forEach var="e" items="${list }">
         
         <tr>
            <td>ID:</td>
            <td>${e.idn }</td>
         </tr>
         <tr>
            <td>IP:</td>
            <td>${e.reip }</td>
         </tr>
         <tr>
            <td>Time:</td>
            <td>${e.sstime }</td>
         </tr>   
         <tr><td colspan="2">============================================================</td></tr>      
      </c:forEach>
   </table>
</div>
</div>
</div>
</div>
<%-- join.jsp의 content영역 끝  --%>
<%@include file="footer.jsp"%>