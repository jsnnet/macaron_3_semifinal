<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>

<br>
<br>
<br>
<br>
<br>
<br>

<script>

$(function() {
	alert('${msg}');
}); 


</script>
<div class="content">

<style>
#list {
	color: black;
	border: solid 1px;
	text-align: center;
	width: 800px;
	/* 	margin-left: 350px; */
	margin: auto;
}

#store_list {
	/* 	border: 10px solid transparent; */
	text-align: center;
	table-layout: fixed;
	/* border: 1px solid; */
}

#td_menu {
	vertical-align: bottom;
}
</style>
	
	<div id="list">
		<fieldset style="border: 1px solid rgba(255, 255, 255, .5);"
			id="container">
			<a style="height: 300px">${sessionScope.uid}님의 결제 페이지</a>

			<table class="table table-hover" id="store_list">
				<tbody>
					<tr>
						<td>음식 사진</td>
						<td><img src="./resources/imgfile/${pvo.foodimg}" style="width=100px; height=100px"></td>
					</tr>
					<tr>
						<td>구매 상품</td>
						
						<td>${pvo.foodname}</td>
					</tr>
					<tr>
						<td>상세 설명</td>						
						<td>${pvo.fooddetail}</td>
					</tr>
					<tr>
						<td>음식 종류</td>						
						<td>${pvo.category}</td>
					</tr>
					<tr>
						<td>상품 금액</td>
						<td>${pvo.foodpay}</td>
					</tr>
					<tr>
						<td>상품 수량</td>
						<td>${pvo.foodtotal}</td>
					</tr>
					<tr>
						<td>결제 방식</td>
						<td>
						<c:choose>
								<c:when test="${pvo.dok eq 0}">배달</c:when>
								<c:when test="${pvo.dok eq 1}">직접 수령</c:when>
							</c:choose>
							</td>
					</tr>
					<tr>
						<td>최종 결제</td>
						<td>${pvo.pay_total}</td>
					</tr>
					<tr>
						<td id="td_menu">
						<form action="payment" method="post">
						<input type="hidden" name="stnum" value="${pvo.stnum}">
						<input type="hidden" name="pay_total" value="${pvo.pay_total}">						
						<input type="hidden" name="foodname" value="${pvo.foodname}">
						<input type="hidden" name="foodpay" value="${pvo.foodpay}">
						<input type="hidden" name="foodtotal" value="${pvo.foodtotal}">
						<input type="hidden" name="dok" value="${pvo.dok}">
						<input type="hidden" name="category" value="${pvo.category}">
						<input	class="btn btn-outline-dark" id="pay" type="submit"
								value="결제하기" style="width: 100%; height: 100%">
						</form>						
						</td>
						<td id="td_menu"> 
						<form action="dutch_pay" method="post">
						<input type="hidden" name="stnum" value="${pvo.stnum}">
						<input type="hidden" name="pay_total" value="${pvo.pay_total}">						
						<input type="hidden" name="foodname" value="${pvo.foodname}">
						<input type="hidden" name="foodpay" value="${pvo.foodpay}">
						<input type="hidden" name="foodtotal" value="${pvo.foodtotal}">
						<input type="hidden" name="dok" value="${pvo.dok}">
						<input	class="btn btn-outline-dark" id="pay" type="submit"
								value="더치페이" style="width: 100%; height: 100%">
						</form>	
						</td>
					</tr>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</fieldset>
		<div class="row" id="menudiv"></div>
	</div>



	<%@include file="footer.jsp"%>