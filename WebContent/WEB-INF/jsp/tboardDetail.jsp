<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="header.jsp" %>

<br>
<br>
<br>
<br>
<br>
<br>
<div id="article">
	<div id="content">
	<fieldset style="width: 600px; margin: auto; color: black;">
	<legend>자유게시판 ${v.postsnum }번째 이야기</legend>
		<div>
				
						<%-- <td>${v.postsnum }</td>--%>
						
						<label>제목 :</label> ${v.title} 
						 | ${v.pdate } | 
					
						<p>
						<label>작성자 :</label> <td>${v.memid}</td>
						</p>
						
						<p>
						<a href="storeDetail?stnum=${v.stnum }">가게 자세히 보기</a>
						</p>
						
						<div>
						<label>사진 :</label> 
						<img src="./resources/imgfile/${v.img}" style="width: 100px" class="absolute">
						</div>
						<br><br>
						
						<label>|| 내용 ||</label><br>
						<td>${v.contents}</td>
					  
						<br><br><br>
					    <p style="text-align: right;">
						<label>조회수 :</label><td>${v.hit} |</td> 
						<label>댓글 수 :</label><td>${v.cnt} |</td> 
						<label>평점 :</label><td>${v.grade } 점</td>
					    </p>
					    
				</tr>
				
				<td >
				<p style="text-align: right;">
				<c:if test="${sessionScope.uid eq v.memid}">
				<a href="updateBorad?postsnum=${v.postsnum }">수정</a>
			    <a href="tboardDelete?postsnum=${v.postsnum }">삭제</a>
			    </c:if>
			    </p>
		        </td>
			
			</div>
		    <br>
				<tr>
				<p style="text-align: right;">
					<th colspan="5" >
					<input type="button" value="목록" id="wBtn"><br>
					</th>
				</p>
				</tr>
				
           
                
				<%--댓글 작성폼 postsnum = tnum--%>
				<legend></legend>
				<form action="tboardAddComm" method="post">
					<input type="hidden" name="tnum" value="${v.postsnum}"> 
					작성자: <input type="text" name="memid" id="memid" value="${sessionScope.uid }" readonly="readonly"><br><br> 
					<textarea rows="5" cols="60" name="comments"></textarea>
					<p style="text-align: right;">
					<input type="submit" value="등록">
					</p> 
				</form>
                <br>
         
				<%--댓글리스트 --%>
				<legend></legend>
				<c:forEach var="e" items="${cvlist}">
					<%-- <p>${e.postsnum}|${e.tnum}|${e.memid}|${e.comments}|${e.tdate}</p> --%>
					<td>${e.memid}|</td>|${e.tdate}
					<c:if test="${sessionScope.uid eq e.memid}">
					<a href="commUpBoradform?postsnum=${e.postsnum }">수정</a>|
					<a href="commDelete?postsnum=${e.postsnum}&postsnum2=${v.postsnum}">삭제</a>
					</c:if>
					<p>${e.comments}</p>
					<legend></legend>
				</c:forEach>
			
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(function(){
		$('#wBtn').click(function(){
			location="boardUplist?page=1";
		});
	});
</script>
</div>
 <%@include file="footer.jsp" %>