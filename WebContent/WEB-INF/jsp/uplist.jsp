<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<br>
<br>
<br>
<br>
<br>
<br>
   <style>
#list {
   color: black;
   border: solid 1px;
   text-align: center;
   width: 700px;
/*    margin-left: 350px; */
   margin: auto;
}
</style>
   <div id="list">
 <fieldset style="border: 1px solid rgba(255, 255, 255, .5);" id="container">
	<table style="width: 100%;">
		<tr >
			<td>음식 이름</td>
			<td>음식가격</td>
			<td>이미지</td>
			<td>수정</td>
			<td>삭제</td>
		</tr>

		<c:forEach var="e" items="${list}">
			<tr>
				<c:forEach var="e" items="${e.menu}">
					<td>${e.foodname }</td>
					<td>${e.foodpay }원 </td>

					<td><img
						src="${pageContext.request.contextPath}/resources/imgfile/${e.foodimg }"
						width="100px" height="100px"></td>
					<td>
						<input type="button" value="수정" onclick="location='alter?foodnum=${e.foodnum}'"
						class="btn btn-primary">
					</td>
					<td><input type="button" value="삭제"
						onclick="location='delete?foodnum=${e.foodnum}&'"
						class="btn btn-danger"></td>
				</c:forEach>
			</tr>
		</c:forEach>
		
		<tr>
			<td colspan="5"style="text-align: right;"><input type="button" value="집으로 돌아가기"
				onclick="location='admypage'" class="btn btn-dark"></td>
		</tr>
	</table>
</fieldset>
</div>

<%@include file="footer.jsp"%>