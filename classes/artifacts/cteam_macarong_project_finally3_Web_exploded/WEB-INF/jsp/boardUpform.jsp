<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<br>
<br>
<br>
<br>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js">
</script>

<script>
	$(function() {
		var total = 1;
		$('#grade1 a').click(function() {
			if ($(this).attr("href") === "#1") {
				total = 1 * 1;
			} else if ($(this).attr("href") === "#2") {
				total = 2 * 1;
			} else if ($(this).attr("href") === "#3") {
				total = 3 * 1;
			} else if ($(this).attr("href") === "#4") {
				total = 4 * 1;
			} else if ($(this).attr("href") === "#5") {
				total = 5 * 1;
			} else {

			}
			
			$('#target').html("Total : " + total);
			$('#grade').val(total);
			
			$(this).parent().children("a").removeClass("on");
			$(this).addClass("on").prevAll("a").addClass("on"); 
			return false;
		});

	});
</script>


<style>

a {
	text-decoration: none;
	color: #696E74;
}

#grade1 a.off {
	text-decoration: none;
	color: #D5D5D5;
}

#grade1 a.on {
	color: #A79A74;
}

</style>
<div id="boardUpform">
	<fieldset style="text-align: center">
		<legend>게시물 등록하기</legend>
		<div id="content">

			<form method="post" action="boardUpsave"
				enctype="multipart/form-data">

				<input type="hidden" name="pay_num" id="pay_num"
					value="${pvo.pay_num}">
<!--  

POSTSNUM NOT NULL NUMBER       
MEMID             VARCHAR2(30)  o 
GRADE             NUMBER       o
STNUM             NUMBER       o
PAY_NUM           NUMBER       o
TITLE             VARCHAR2(50)   o 
CONTENTS          CLOB         o 
IMG               VARCHAR2(30) 
HIT               NUMBER       
PDATE             DATE      
upfile ?  
-->
				<table style="margin-left: auto; margin-right: auto;">

					<tr>
						<td style="text-align: left;">제 목 :</td>
					</tr>

					<tr>
						<td style="text-align: left;">
						<input type="text" name="title" required="required"></td>
					</tr>


					<tr>
						<td style="text-align: left;">작성자:</td>
					</tr>

					<tr>
						<td style="text-align: left;"><input type="text" name="memid"
							id="memid" value="${sessionScope.uid }" readonly="readonly"></td>
					</tr>

					<tr>
						<td style="text-align: left;">평 점:</td>
					</tr>
					<tr>
						<td style="text-align: left;">
							<p id="grade1">
								<a href="#1" class="a.off">★</a> 
								<a href="#2" class="a.off">★</a>
								<a href="#3" class="a.off">★</a> 
								<a href="#4" class="a.off">★</a>
								<a href="#5" class="a.off">★</a> 
								<span id="target">Total:</span>
							</p>
						</td>
					</tr>
					
					<tr>
						<td style="text-align: left;">
						<input type="text" name="grade" id = "grade" value="0"/>
						</td>
					</tr>

					<tr>
						<td style="text-align: left;">가게 번호:</td>
					</tr>
					
					<tr>
						<td style="text-align: left;">
						<input type="text" name="stnum"
						value="${pvo.stnum}" readonly="readonly">
						</td>
					</tr>


					<tr>
						<td style="text-align: left;">주문번호:</td>
					</tr>
					<tr>
						<td style="text-align: left;"><input type="text"
							name="pay_num" value="${pvo.pay_num}" readonly="readonly"></td>
					</tr>

					<tr>
						<td style="text-align: left;">이미지:</td>
					</tr>

					<tr>
						<td><input type="file" name="upfile"></td>
					</tr>

					<tr>
						<td style="text-align: left;">작성 내용:</td>
					</tr>

					<tr>
						<td><textarea rows="5" cols="60" name="contents" required="required"></textarea></td>
					</tr>

					<tr>
						<td style="text-align: right;">
						<input type="submit" value="Send">
						<td>
					</tr>
				</table>
			</form>

		</div>
	</fieldset>
</div>

<%@include file="footer.jsp"%>