<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="header.jsp"%>
<script>
	function commboardUpdate() {
		location.href = "commUpBoradform?num=" + ${tvo.postsnum};
	
	}
</script>
<br>
<br>
<br>
<br>
<br>
<div id="commupdateForm">
	<fieldset style="text-align: center">
		<legend>게시물 수정 페이지</legend>
		<div id="content">
			<form method="post" action="commboardUpdate"
				enctype="multipart/form-data">
				<input type="hidden" name="postsnum" id="postsnum"
					value="${tvo.postsnum}">
				<input type="hidden" name="tnum" id="tnum"
					value="${tvo.tnum}">	
				<table style="margin-left: auto; margin-right: auto;">

					<tr>
						<td style="text-align: left;">작성자: ${tvo.memid}</td>
					</tr>
					
					<tr>
						<td style="text-align: left;">
						<input type="text" value="${tvo.memid}" name="memid" readonly="readonly"></td>
					</tr>
					
					<tr>
						<td style="text-align: left;">${tvo.tnum}이야기에 작성된 댓글입니다.</td>
					</tr>


					<tr>
						<td style="text-align: left;">기존 내용 : ${tvo.comments}</td>
					</tr>

					<tr>
						<td style="text-align: left;">작성내용 :</td>
					</tr>
					<tr>
						<td style="text-align: left;"><textarea rows="5" cols="60"
								name="comments" id="comments">${tvo.comments}</textarea>
					<br>
					</td>
					</tr>

					<tr>
						<td><input type="submit" value="Send"></td>
					</tr>

				</table>
			</form>
		</div>
	</fieldset>
</div>
<%@include file="footer.jsp"%>