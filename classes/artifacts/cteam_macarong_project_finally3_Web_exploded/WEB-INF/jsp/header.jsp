<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>cteam_마카롱 프로젝트</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Favicons -->
<link href="resources/img/favicon.png" rel="icon">
<link href="resources/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic-ext"
	rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="resources/lib/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="resources/lib/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<link href="resources/lib/ionicons/css/ionicons.min.css"
	rel="stylesheet">
<link href="resources/lib/photostack/photostack.css" rel="stylesheet">
<link href="resources/lib/fullpage-menu/fullpage-menu.css"
	rel="stylesheet">
<link href="resources/lib/cubeportfolio/cubeportfolio.css"
	rel="stylesheet">
<link href="resources/lib/superslides/superslides.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="resources/css/style.css"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- =======================================================
    Template Name: Polaroyd
    Template URL: https://templatemag.com/polaroyd-bootstrap-agency-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
<style>
	
	#inter{
			position: fixed; 
			right: 20px; 
			bottom:150px; 
			width: 200px; 
			background-color: white;
	}

</style>
</head>
<script>
$(document).ready(function(){
	<c:if test="${sessionScope.uid == null && sessionScope.stid == null}">
		$('#menu1').addClass("active");
	</c:if> 
	<c:if test="${sessionScope.uid == null && sessionScope.stid != null}">
		$('#menu2').addClass("active");
	</c:if> 
	<c:if test="${sessionScope.uid != null && sessionScope.stid == null}">
		$('#menu1').addClass("active");
	</c:if> 
	
	// 세션의 회원 넘버 = unum
	var unum = "<%=session.getAttribute("unum") %>";
	
	<c:choose>
		<c:when test="${sessionScope.uid != null }">
			$('#inter').show();
			$.ajax({
				url:"./store/interestmem?unum="+unum,
				dataType : 'json',
				cache : false,
				success:function(data){
					let tag2 ='<span class="glyphicon glyphicon-star" aria-hidden="true">&nbsp; ${sessionScope.uname }님의 선호도</span>';
					let path = '${pageContext.request.contextPath }';
					var stsbname;
					var index;
					$.each(data,function(idx, key){
						stsbname = key.stsbname;
						index = (idx+1);
						$.each(key.menu,function(idx, key){
							tag2 += '<div id="latelyview1'+key.stnum+'" class ="row" style ="width: 200px; height: 200px;">';
							tag2 += '<table class="table" style="text-align: center;">';
							tag2 += '<tr><td>';
							tag2 += '선호도 순위 : ' + index;
							tag2 += '</td></tr>';
							tag2 += '<tr><td>';
							tag2 += '가게 이름 : ' + stsbname;
							tag2 += '</td></tr>';
							tag2 += '<tr><td>';
							tag2 += '<a href="storeDetail?stnum=' + key.stnum + '">';
							tag2 += "<img src="+path+"/resources/imgfile/"+key.foodimg+".jpg";
							tag2 += ' style ="width: 200px; height: 100px;"></a>';
							tag2 += '</td></tr>';
							tag2 += '</table>';
							tag2 += '</div>';
						}
						)
					})
					$('#inter').html(tag2);
				}
			}) 
		</c:when>
		<c:otherwise>
			$('#inter').hide();
		</c:otherwise>
	</c:choose>
	
})
    function home() {
        var res = confirm("메인 화면으로 이동하십니까?");
        if (res == false) return;
        location.href = "main";
    }
    function memberJoin() {
        location.href = "member";
    }
   
    </script>

<body>

	<!-- Custom navbar -->
	<div class="custom-navbar">
			<a class="fm-button" href="#">Menu</a>
	</div>
	<div class="sidemenu">
		<ul class="fm-first-level">
			<c:choose>
				<c:when test="${sessionScope.uid != null }">
					<li><a href="#menu1">회원 페이지</a></li>
					<li><a href="#menu1" onclick="alert('회원은 접근할 수 없습니다.')">관리자 페이지</a></li>
					<li><a href="#menu1" onclick="alert('로그인된 회원입니다. ')">회원 가입</a></li>
				</c:when>
				<c:when test="${sessionScope.stid != null }">
					<li><a href="#menu2" onclick="alert('관리자는 접근할 수 없습니다.')">회원 페이지</a></li>
					<li><a href="#menu2">관리자 페이지</a></li>
					<li><a href="#menu2" onclick="alert('로그인된 관리자 입니다. ')">회원 가입</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="#menu1">회원 페이지 </a></li>
					<li><a href="#menu2">관리자 페이지</a></li>
					<li><a href="#" onClick="memberJoin()">회원 가입</a></li>
				</c:otherwise>
			</c:choose>
			<li><a href="#" onClick="home()">Home</a></li>
		</ul>
		<nav id="menu1">
			<c:choose>
				<c:when test="${sessionScope.uid == null }">
					<a href="loginForm"><span class="subtitle">로그인 기능 입니다. </span>	로그인</a>
				</c:when>
				<c:otherwise>
					<a href="logout"><span class="subtitle">로그아웃 기능 입니다. </span>${sessionScope.uname }님
							로그아웃</a>
				</c:otherwise>
			</c:choose>
			<a href="myPage"> <span class="subtitle">회원의 마이페이지 입니다.</span> 
					마이 페이지
			</a> <a href="storelistController?page=1"> <span class="subtitle">현재 등록된 마카롱 가게를 확인할 수 있습니다.</span> 마카롱 가게 리스트
			</a> <a href="listBasket2"> <span class="subtitle">회원의 장바구니 페이지 입니다.</span>장바구니
			</a> <a href="boardUplist?page=1"> <span class="subtitle">후기 게시판 입니다. </span> 후기 게시판
			</a> <a href="mailform"><span class="subtitle">We are
               kind people and love to speak with our clients.</span> Q&A
         </a>
		</nav>
		<nav id="menu2">
			<c:choose>
				<c:when test="${sessionScope.stid == null }">
					<a href="stloginForm"><span class="subtitle">점주 로그인 기능입니다. </span> 점주 로그인</a>
				</c:when>
				<c:otherwise>
					<a href="stlogout"><span class="subtitle"> 점주 로그아웃 기능입니다. </span>${sessionScope.stname }님
							로그아웃</a>
				</c:otherwise>
			</c:choose>
			<a href="admypage"> <span class="subtitle">점주의 마이페이지 입니다. </span> 
					마이 페이지
			</a> 
		</nav>
		
	</div>
	<div class="container-fluid" id="inter" >
	</div>
	<!-- /END CUSTOM NAVBAR -->