<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<style>
#target {
	color: red;
	background-color: yellow;
	width: 50%;
	font-size: 18px;
	/* 숨김 속성 */
	display: none;
}
</style>

<%-- loginform.jsp --%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>회원가입</title>
        <!-- Bootstrap -->
        <!-- font awesome -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <!-- Custom style -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

 

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
    </head>
    <body>
	<div id="sex">
		<article class="container">
			<div class="page-header"></div>
			<tr>
				<hr>
				<br>
				<span style="padding-left: 160px"> <img
					src="resources/img/macaron/logo2.png" align="middle">
					</p>
					<div class="col-sm-6 col-md-offset-3">
						<form method="post" action="memberjoin" id="memF">
							<div class="form-group">
                        <label for="id">아이디</label>
                        <input type="text" class="form-control" name="memid" id="memid" placeholder="아이디를입력해주세요" ng-required="true" ng-maxlength="4">
                     &nbsp; <button type="button" id="idChkBtn"  class="btn btn-primary active" value="중복확인">
					   아이디중복확인<i class="fa fa-check spaceLeft"></i>
                    </div>
							<div class="form-group">
								<label for="memname">이름</label> <input type="text"
									class="form-control" name="memname" id="memname"
									placeholder="이름을입력해주세요" ng-required="true" ng-maxlength="4" required="required">
							</div>
							<div class="form-group">
								<label for="mememail">이메일 주소</label> <input type="email"
									class="form-control" name="mememail" id="mememail"
									placeholder="이메일 주소를 입력해주세요" required="required">
							</div>
							<div class="form-group">
								<label for="inputPassword">비밀번호</label> <input type="password"
									class="form-control" name="mempwd" id="mempwd"
									placeholder="비밀번호를 입력해주세요" required="required">
							</div>
							<div class="form-group">
								<label for="inputPasswordCheck">비밀번호 확인</label> <input
									type="password" class="form-control" name="mempwd2"
									id="mempwd2" placeholder="비밀번호 확인을 위해 다시한번 입력 해 주세요" required="required">
							</div>

								<div class="form-group">
                        <label for="date">생일 / 성별</label>
                        <input type="date" class="form-control" id="date" >
                        <input type="radio" name="memgender" id="memgender" class="custom-control-input" value="남자" checked="checked">
						<label class="custom-control-label" for="jb-radio-1">남자</label>
						<input type="radio" name="memgender" id="memgender" class="custom-control-input" value="여자">
						<label class="custom-control-label" for="jb-radio-2">여자</label>
                    </div>


							<div class="form-group">
								<label for="memphone">휴대폰 번호</label> <input type="tel"  
									class="form-control" name="memphone" id="memphone"
									placeholder="휴대폰번호를 입력해 주세요" required="required">
							</div>
							<div class="form-group">
								<label for="inputs">우편번호</label> <input type="text"
									class="form-control" id="mempost" name="mempost"
									placeholder="우편번호를 찾아 주세요" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="juso">주소</label> <input type="text"
									class="form-control" id="memaddr1" name="memaddr1"
									placeholder="주소를 입력해 주세요" required="required" readonly="readonly">
							</div>	
									<div class="form-group">
								<label for="inputs">상세주소</label> <input type="text"
									class="form-control" id="memaddr2" name="memaddr2"
									placeholder="상세주소를  입력해주세요" required="required">
							
								<button type="button" id="postBtn" class="btn btn-info" />
								우편번호<i class="fa fa-check spaceLeft"></i>
							</div>
                 
               
            
                    <div class="form-group text-center">
                        <button type="submit" id="join-submit" class="btn btn-primary">
                            회원가입<i class="fa fa-check spaceLeft"></i>
                        </button>
                        <button type="submit" class="btn btn-warning">
                            가입취소<i class="fa fa-times spaceLeft"></i>
                        </button>
                    </div>
                </form>
            </div>

        </article>
        
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	$(function(){
		$('#postBtn').click(function(){
			 execDaumPostcode();
		});
		// 회원 아이디 중복확인!
		$('#idChkBtn').click(function(){
			if($('#memid').val() == ''){
				alert('값을 입력하여 주세요 .');
			}else{
			$.ajax({
				url:"idcheck?memid="+$('#memid').val(),
				success:function(data){
					if(data == 0){
						alert('사용 가능 ');
						$('#memid').attr("readonly",true);
					}else{
						alert('사용 불가 ');
					}
				}		
			});
			}
		});
		function execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('mempost').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('memaddr1').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('memaddr2').focus();
            }
        }).open();
    }  // fn end
	});
	//특수문자 
function checkNumber()
{
 var objEv = event.srcElement;
 var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/ ";    //입력을 막을 특수문자 기재.
 event.returnValue = true;
  
 for (var i=0;i<objEv.value.length;i++)
 {
 if(-1 != num.indexOf(objEv.value.charAt(i)))
 event.returnValue = false;
 }
  
 if (!event.returnValue)
 {
  alert("특수문자는 입력하실 수 없습니다.");
  objEv.value="";
 }
}
</script>
</html>

<%@include file="footer.jsp" %>