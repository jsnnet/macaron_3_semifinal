<%@ page language="java" contentType="text/html; charset=EUC-KR"
   pageEncoding="EUC-KR"%>
<%@include file="header.jsp" %>
<div class="content">
   <style>
#list {
   color: black;
   border: solid 1px;
   text-align: center;
   width: 800px;
   /*    margin-left: 350px; */
   margin: auto;
}

#store_list {
   /*    border: 10px solid transparent; */
   text-align: center;
   table-layout: fixed;
   /* border: 1px solid; */
}

#td_menu {
   vertical-align: bottom;
}

#menu_modal {
   /* width: auto; */
   height: 100%;
   width: 100%;
}

.td_menu {
   width: auto;
}
</style>

<h2 class="title">메인페이지</h2>
  <div class="span9" id="content">
                    <div class="row-fluid">
            
           <div class="alert alert-warning">
               <h2 class="sect_tit" align="senter">${sessionScope.uname }님의 마이페이지</h2>   
               
                     <p>이름 :
                     ${sessionScope.uname}</p>
                  
                     <p>주소 :
                     ${mvo.memaddr1}${mvo.memaddr2}</p>
               
            </h2>
         </div>
         <!--정보 수정  -->
  <ul class="row hidden-sm clients mt2">
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
          <a href="my_update" data-toggle="tooltip"    title="회원 정보 수정"> 
        <img alt="" src="resources/img/logos/my.png" title="회원 정보 수정">
      </figure>
   </li>
   
   <!--더치 페이    -->
     <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
      <a id="menu_modal" data-toggle="tooltip" onclick="location='dutch_status'" 
                     title="더치페이상황"> 
         <img alt="" src="resources/img/logos/du.png" title="" class="img-responsive">
      </a>
      </figure>
   </li>
    
   <!--친구 리스트 -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
           <a id="menu_modal" data-target="#layerpop_list" data-toggle="modal"> 
            <p data-toggle="tooltip" title="친구 리스트 "> 
         <img alt=""   src="resources/img/logos/friend.png" title="" class="img-responsive">
         </a>
         </figure>
     </li>
            
   <!--로그 보기   여기 수정해야해 -->   
           <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
       <a href="mylog" data-toggle="tooltip" title="로그보기  "> 
       <img   alt="" src="resources/img/logos/log.png" title="" class="img-responsive">
      </a>
      </figure>
      </li>
      </ul>
    <ul class="row hidden-sm clients mt2">
   
    <!--구매 이력  -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
           <a id="menu_modal" data-target="#layerpop_search" data-toggle="modal"> 
            <p data-toggle="tooltip" title="구매 이력  "> 
         <img alt=""   src="resources/img/logos/dddd.png" title="" class="img-responsive">
         </a>
         </figure>
     </li>
      
      <!--친구대기  -->
      <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
          <a id="menu_modal" onclick="location='friendWait'" data-toggle="tooltip" title="친구대기 (${cnt})"> <img
                     alt="" src="resources/img/logos/wait.png" title=""
                     class="img-responsive">
                  </a>
               </figure>
            </li>
   
   <!--친구 신청  -->
     <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
                 <a id="menu_modal" data-toggle="tooltip" onclick="location='friendapply'" title="친구 온 사람들(${cnt_apply})"> <img
                     alt="" src="resources/img/logos/sin.png" title=""
                     class="img-responsive">
                  </a>
               </figure>
            </li>
   
   
   <!--메인  -->  
   
   <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
           <a id="menu_modal" data-target="#layerpop_friendplus" data-toggle="modal"> 
            <p data-toggle="tooltip" title="친구 신청 "> 
         <img alt=""   src="resources/img/logos/main.png" title="" class="img-responsive">
         </a>
         </figure>
     </li>          
     
    <!-- .banner-wrap (end) -->
  </ul>
  </div>
  </div>
<div class="modal fade" id="layerpop_list"  style="text-align: center;">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">      
          ${sessionScope.uname}님의 친구리스트 입니다
               <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            
            <div class="modal-body" >
       
               <table>
                  <c:forEach var="e" items="${friend_list}">
                     <th>
                       . ${e}님              
                     </th>
                  </c:forEach>
               </table>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
            </div>
         </div>
      </div>
   </div>
</div>


<!--  -->


<div class="modal fade" id="layerpop_search">
	<div class="modal-dialog" style="max-width: 80%; width: auto;">
		<div class="modal-content">
			<div class="modal-header">
				<p>${sessionScope.uname}님의 구매 이력
				<button type="button" class="close" data-dismiss="modal">×</button></p>
			</div>
			<div class="modal-body">
				<table style="width: 100%">
				
			<tr><td><a href="myReviewList?page=1">내가 쓴 리뷰 보기</a></td></tr>
				
					<tr>
						<td class="td_menu">가게 이름</td>
						<td class="td_menu">구매 시간</td>
						<td class="td_menu">결제 금액</td>
						<td class="td_menu">결제 음식</td>
						<td class="td_menu">음식 수량</td>
						<td class="td_menu">리뷰등록</td>
						<td class="td_menu">더치페이 유무</td>
						<td class="td_menu">배송 유무</td>
					</tr>
					<c:forEach var="ex" items="${pay_list}">

						<tr>
							<td>${ex.store.stsbname}</td>
							<td>${ex.pay_time}</td>
							<td>${ex.pay_total}</td>
							<td>${ex.foodname}</td>
							<td>${ex.foodtotal}</td>
							<td><a href="boardUpform?pay_num=${ex.pay_num}">리뷰작성하기</a></td>
							
							<td><c:choose>
									<c:when test="${ex.dutch_ok eq 0}">일반 구매</c:when>
									<c:when test="${ex.dutch_ok eq 1}">더치페이</c:when>
								</c:choose></td>
							<td><c:choose>
									<c:when test="${ex.dok eq 0}">배달</c:when>
									<c:when test="${ex.dok eq 1}">직접 수령</c:when>
								</c:choose></td>

						</tr>
						
					</c:forEach>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>



<!-- 친구 추가 -->


<div class="modal fade" id="layerpop_friendplus" style="text-align: center;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
         </div>
         <div class="modal-body">
            <legend>친구 추가</legend>
            <form method="post" action="friendplus" autocomplete="off"
               enctype="multipart/form-data">
               <input type="hidden" name="stnum" id="stnum" value="">
               <p>
                  <label>친구검색</label> <input type="text" name="friendid"
                     id="friendid">
            

           
                  <input type="submit" value="등록" style="text-align: right; height: 28px;" 
                   class="btn btn-info" >
               </p>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
         </div>
      </div>
   </div>
</div>



<%@include file="footer.jsp"%> 