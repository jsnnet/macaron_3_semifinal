<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="header.jsp" %>

<style>
#content th,td,tr {
	color: #000;
	padding: 10px;
}

#content a:link {
	color: #000000;
	text-decoration: none;
}

</style>

<br>
<br>
<br>
<br>
<br>

<div id="orderList">
<div class="modal-dialog" style="max-width: 80%; width: auto;">
		<div class="modal-content">
			<div class="modal-header">
				<p>${sessionScope.uname}님의 구매 이력
			</div>
			<div class="modal-body">
			
				<table style="width: 100%">
					<tr>
						<td class="td_menu">가게 이름</td>
						<td class="td_menu">가게 번호</td>
						<td class="td_menu">상품번호</td>
						<td class="td_menu">구매 시간</td>
						<td class="td_menu">결제 금액</td>
						<td class="td_menu">결제 음식</td>
						<td class="td_menu">음식 수량</td>
						<td class="td_menu">리뷰란</td>
					</tr>
					<c:forEach var="ex" items="${pay_list}">
						<tr>
							<td>${ex.store.stsbname}</td>
							<td>${ex.store.stnum}</td>
							<td>${ex.pay_num}</td>
							<td>${ex.pay_time}</td>
							<td>${ex.pay_total}</td>
							<td>${ex.foodname}</td>
							<td>${ex.foodtotal}</td>
							<td><a href="boardUpform?pay_num=${ex.pay_num}">리뷰작성하기</a></td>
						</tr>
						
					</c:forEach>
				</table>
			</div>
		</div>
</div>
</div>

 <%@include file="footer.jsp" %>