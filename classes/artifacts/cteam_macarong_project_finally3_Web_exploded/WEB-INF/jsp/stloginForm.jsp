<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%-- loginform.jsp --%>
<%@include file="header.jsp" %>
<html lang="en">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Style -->
    <link href="css/style.css" rel="stylesheet">
 
  </head>
  <body>
  <fieldset style="width: 300px; margin: auto; color: black;">
    <div class="container">
      <div class="row">
        <div class="page-header">
        </div>
        <div class="col-md-3">
          <div class="login-box well">
			<form method="post" action="stloginProcess" autocomplete="off"> <legend>관리자로그인 ${error }</legend>
               <input type="hidden" name="method" id="method" value="${method }">
               <p> <label> 아이디</label>
                <input type="text" name="stid" id="stid" placeholder=" Username"  />
                </p>
               	<p> <label for="stpwd">비밀번호</label>
                <input type="password" name="stpwd" id="stpwd" placeholder="Password"/>
                </p>
	            <div class="form-group">
                <input type="submit" class="btn btn-default btn-login-submit btn-block m-t-md" value="Login" />
        
            </div>
            <span class='text-center'><a href="pas" class="text-sm">비밀번호 찾기</a></span>
            <hr />
            <div class="form-group">
                <a href="member" class="btn btn-default btn-block m-t-md"> 회원가입</a>
            </div>
        </form>
        </fieldset>
          </div>
        </div>
      </div>
    </div>
 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  </body>
  </html>
</div>
<%@include file="footer.jsp" %>
