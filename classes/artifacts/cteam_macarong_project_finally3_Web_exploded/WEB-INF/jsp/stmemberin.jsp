<%@ page language="java" contentType="text/html; charset=EUC-KR"
   pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>

<%-- loginform.jsp --%>
<style>
#joinform {
   width: 70%;
   margin: auto;
}
</style>
<link
   href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
   rel="stylesheet">
<script
   src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
</head>
<body>
   <div id="joinform">
      <article class="container">
         <div class="page-header"></div>
         <tr>
            <hr>
            <br>
            <span style="padding-left: 160px"> <img
               src="resources/img/macaron/logo2.png" align="middle">
               </p>
               <div class="col-sm-6 col-md-offset-3">
                  <form method="post" action="storejoin" id="stmember">
                  <div class="form-group">                  
                  <label for="id">가게이름</label> <input type="text"
                           class="form-control" name="stsbname" id="stsbname"
                           placeholder="가게이름을입력해주세요" ng-required="true" ng-maxlength="4">
                        <button type="button" id="sstidChkBtn"
                           class="btn btn-primary active" value="중복확인">
                           중복확인<i class="fa fa-check spaceLeft"></i>
                  </div>
                  <div class="form-group">
                  <label for="id">아이디</label> <input type="text"
                           class="form-control" name="stid" id="stid"
                           placeholder="아이디를입력해주세요" ng-required="true" ng-maxlength="4">
                        <button type="button" id="stidChkBtn"
                           class="btn btn-primary active" value="중복확인">
                           아이디중복확인<i class="fa fa-check spaceLeft"></i>
                     </div>
                     <div class="form-group">
                        <label for="stname">이름</label> <input type="text"
                           class="form-control" name="stname" id="stname"
                           placeholder="이름을입력해주세요" ng-required="true" ng-maxlength="4">
                     </div>
                     <div class="form-group">
                        <label for="stemail">이메일 주소</label> <input type="email"
                           class="form-control" name="stemail" id="stemail"
                           placeholder="이메일 주소를 입력해주세요">
                     </div>
                     <div class="form-group">
                        <label for="stpwd">비밀번호</label> <input type="password"
                           class="form-control" name="stpwd" id="stpwd"
                           placeholder="비밀번호를 입력해주세요">
                     </div>
                     <div class="form-group">
                        <label for="stpwd2">비밀번호 확인</label> <input
                           type="password" class="form-control" name="stpwd2"
                           id="stpwd2" placeholder="비밀번호 확인을 위해 다시한번 입력 해 주세요">
                     </div>

                     <div class="form-group">
                        <label for="stphone">휴대폰 번호</label> <input type="tel"  
                           class="form-control" name="stphone" id="stphone"
                           placeholder="휴대폰번호를 입력해 주세요">
                     </div>
                     <div class="form-group">
                        <label for="post">우편번호</label> <input type="text"
                           class="form-control" id="stpost" name="stpost"
                           placeholder="우편번호를 찾아 주세요" readonly="readonly">
                     </div>
                     <div class="form-group">
                        <label for="addr1">주소</label> <input type="text"
                           class="form-control" id="staddr1" name="staddr1"
                           placeholder="주소를 입력해 주세요">
                     </div>   
                           <div class="form-group">
                        <label for="addr2">상세주소</label> <input type="text"
                           class="form-control" id="staddr2" name="staddr2"
                           placeholder="상세주소를  입력해주세요">
                        <button type="button" id="postBtn" class="btn btn-info">
                        우편번호<i class="fa fa-check spaceLeft"></i>
                        </button>
                     </div>
                     
                           <div class="inputArea">
                            <label for="stimg">가게 이미지</label>
                   <input type="file" id="stimg" name="stimg" />
                      <div class="select_img"><img src="" /></div>
                      </div>
                     
                     
                     <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">
                           회원가입<i class="fa fa-check spaceLeft"></i>
                        </button>
                        <button type="submit" class="btn btn-warning">
                           가입취소<i class="fa fa-times spaceLeft"></i>
                        </button>
                     </div>
                  </form>
               </div>
   </div>
</body>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
$(function(){
   //비밀번호 확인
      $('#stpwd2').blur(function(){
         if($('#stpwd').val() != $('#stpwd2').val()){
             if($('#stpwd2').val()!=''){
             alert("비밀번호가 일치하지 않습니다.");
                 $('#stpwd2').val('');
                $('#stpwd2').focus();
             }
          }
      });    
   });
   $(function() {
      $('#postBtn').click(function() {
         execDaumPostcode();
      });
      // 회원 아이디 중복확인!
      $('#idchkbtn').click(function() {
         $.ajax({
            url : "idcheck?memid=" + $('#memid').val(),
            success : function(data) {
               if (data == 0) {
                  $('#target').show().css("color", "blue").text("사용가능");
               } else {
                  $('#target').show().css("color", "red").text("사용중");
               }
            }
         });
      });
      function execDaumPostcode() {
         new daum.Postcode({
         oncomplete: function(data) {
         // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

         // 각 주소의 노출 규칙에 따라 주소를 조합한다.
         // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
         var fullAddr = ''; // 최종 주소 변수
         var extraAddr = ''; // 조합형 주소 변수

         // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
         if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
         fullAddr = data.roadAddress;

         } else { // 사용자가 지번 주소를 선택했을 경우(J)
         fullAddr = data.jibunAddress;
         }

         // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
         if(data.userSelectedType === 'R'){
         //법정동명이 있을 경우 추가한다.
         if(data.bname !== ''){
         extraAddr += data.bname;
         }
         // 건물명이 있을 경우 추가한다.
         if(data.buildingName !== ''){
         extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
         }
         // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
         fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
         }

         // 우편번호와 주소 정보를 해당 필드에 넣는다.
         document.getElementById('stpost').value = data.zonecode; //5자리 새우편번호 사용
         document.getElementById('staddr1').value = fullAddr;

         // 커서를 상세주소 필드로 이동한다.
         document.getElementById('staddr2').focus();
         }
         }).open();
      } // fn end
   });
   //특수문자 
   function mempwd() {
      var objEv = event.srcElement;
      var num = "{}[]()<>?_|~`!@#$%^&*-+\"'\\/ "; //입력을 막을 특수문자 기재.
      event.returnValue = true;

      for (var i = 0; i < objEv.value.length; i++) {
         if (-1 != num.indexOf(objEv.value.charAt(i)))
            event.returnValue = false;
      }
      if (!event.returnValue) {
         alert("특수문자는 입력하실 수 없습니다.");
         objEv.value = "";
      }
   };
   
     $("#stimg").change(function(){
      if(this.files && this.files[0]) {
       var reader = new FileReader;
       reader.onload = function(data) {
        $(".select_img img").attr("src", data.target.result).width(500);        
       }
       reader.readAsDataURL(this.files[0]);
      }
     });
      $('#stidChkBtn').click(function() {
         if ($('#stid').val() == '') {
            alert('값을 입력하여 주세요 .');
         } else {
            $.ajax({
               url : "stidchk?stid=" + $('#stid').val(),
               success : function(data) {
                  if (data == 0) {
                     alert('사용 가능 ');
                     $('#stid').attr("readonly", true);
                  } else {
                     alert('사용 불가 ');
                  }
               }
            });
         }
      });
      $('#sstidChkBtn').click(function() {
         if ($('#stsbname').val() == '') {
            alert('값을 입력하여 주세요 .');
         } else {
            $.ajax({
               url : "sstidchk?stsbname="+$('#stsbname').val(),
               success : function(data) {
                  if (data == 0) {
                     alert('사용 가능 ');
                     $('#stsbname').attr("readonly", true);
                  } else {
                     alert('사용 불가 ');
                  }
               }
            });
         }
      });
</script>

<%@include file="footer.jsp"%>