<%@ page language="java" contentType="text/html; charset=EUC-KR"
   pageEncoding="EUC-KR"%>
<%-- loginform.jsp --%>
<%@include file="header.jsp"%>
<br>
<br>
<br>
<br>
<br>
<br>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e90cdeec53794118950db07f3e598779&libraries=services"></script>

<script>
// 지역 선택에 대한 select tag가 변한다면 실행되는 함수
   function sChangeCategory1() {
      let sido1 = $('#sido').val();
      $.ajax({
         // 시,도 선택에 대한 해당 지역을 ajax 처리로 비동기 처리 해준다.
         url : "./store/sido?sido=" + $('#sido').val(),
         dataType : 'json',
         cache : false,
         success : function(data) {
            let tag = "<option value=''>구/군 선택</option>";
            $.each(data, function(idx, key) {
               tag += "<option value=";
                 tag+=key;
                 tag+=">"
                     + key;
               tag += "</option>";
            });
            $('#gugun').html(tag);
         }
      });
   }
   // 상세 주소에 대한 검색을 누를시 실행되는 함수
   function search(url){
      // 자바스크립트를 이용하여 문서에 HTML form 요소를 추가
      // 여기서 생성된 form 태그에 hidden 값으로 post 요청을 보낸다.
      var form = document.createElement("form");
      // param = 파라미터로 던져질 key, value 값
      var param = new Array();
      // input 태그로 만들어 줄 배열
      var input = new Array();
      
      let sido = $('#sido').val();      // search_addr1 = 선택한 시, 도 에 대한 값 
      let gugun = $('#gugun').val(); // search_addr2 = 선택한 구, 군 에 대한 값
      let search_addr3 = $('#search').val(); // 구체적으로 검색하는 주소 // search_addr3
      
      if(sido == '선택' ){
         alert('지역을 선택하여 주세요. ');
         return;
      }else if(gugun == ''){
         // 시도를 선택했다면
         form.action = url;
         form.method = 'post';
         param.push(['page','1']);
         param.push(['searchType','3']);
         param.push(['searchValue', sido])
         // searchType 주소 = 3 / 상세 주소가 있다면 searchType = 4 /  
         // 지역 staddr1 , 구군 staddr1
         for(var i = 0; i < param.length; i++){
            input[i] = document.createElement('input');
            // <input type="hidden" name="page" value = '1'> 이런식으로 만들어 준다.
            input[i].setAttribute('type','hidden');
            input[i].setAttribute('name',param[i][0]);
            input[i].setAttribute('value',param[i][1]);
            // 생성되어진 form 태그에 자식 요소로 추가
            form.appendChild(input[i]);
         }
         // html body 요소에 자식요소로 넣어준다.
         document.body.appendChild(form);
         // 생성된 form 을 sumit() 해준다. 
         form.submit();
         return;
      // 구,군과 상세주소 모두 입력하였을때
      }else if(gugun != '' && search_addr3 != ''){
         form.action = url;
         form.method = 'post';
         param.push(['page','1']);
         param.push(['searchType','5']);
         param.push(['searchValue', sido]);
         param.push(['searchValue2', gugun]);
         param.push(['searchValue3', search_addr3]);
         
         for(var i = 0; i < param.length; i++){
            input[i] = document.createElement('input');
            input[i].setAttribute('type','hidden');
            input[i].setAttribute('name',param[i][0]);
            input[i].setAttribute('value',param[i][1]);
            form.appendChild(input[i]);
         }
         document.body.appendChild(form);
         form.submit();
         return;
      // 구,군은 선택 했지만 상세 주소는 선택하지 않았을 때
      }else if(gugun != ''){
         form.action = url;
         form.method = 'post';
         param.push(['page','1']);
         param.push(['searchType','4']);
         param.push(['searchValue', sido]);
         param.push(['searchValue2', gugun]);
         
         for(var i = 0; i < param.length; i++){
            input[i] = document.createElement('input');
            input[i].setAttribute('type','hidden');
            input[i].setAttribute('name',param[i][0]);
            input[i].setAttribute('value',param[i][1]);
            form.appendChild(input[i]);
         }
         document.body.appendChild(form);
         form.submit();
         return;
      }
      
   }
   // 카카오 맵api 사용
   window.onload = function(){
      var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
       mapOption = {
           center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
           level: 11 // 지도의 확대 레벨
       };  
   // 지도를 생성합니다    
   var map = new kakao.maps.Map(mapContainer, mapOption); 
   // 주소-좌표 변환 객체를 생성합니다
   var geocoder = new kakao.maps.services.Geocoder();
   // db 에 등록된 가게들의 주소 모두를 뽑아와 마커로 만들어 줄때
   // jstl 문법을 사용한다. 
   <c:forEach var="e" items="${list2 }">
   // 주소로 좌표를 검색합니다
   geocoder.addressSearch('${e.staddr1 } ${e.staddr2 }', function(result, status) {
       // 정상적으로 검색이 완료됐으면 
        if (status === kakao.maps.services.Status.OK) {
           var coords = new kakao.maps.LatLng(result[0].y, result[0].x);
           // 결과값으로 받은 위치를 마커로 표시합니다
           var marker = new kakao.maps.Marker({
               map: map,
               position: coords,
               title: '${e.stnum}'
           });
           // 인포윈도우로 장소에 대한 설명을 표시합니다
           var infowindow = new kakao.maps.InfoWindow({
               content: '<div style="width:100px;text-align:center;padding:6px 0;">${e.stsbname }</div>'
           });
           infowindow.open(map, marker);
           // 지도의 중심을 결과값으로 받은 위치로 이동시킵니다
           map.setCenter(coords);
       } 
       // 각 주소에 대한 마커가 생성되고, 클릭되었을시 상세 페이지로 이동하는 함수
       kakao.maps.event.addListener(marker, 'click', function() {
            // 마커 위에 인포윈도우를 표시합니다
           detail(marker.getTitle())
      });
   });  
   </c:forEach>

   }
   // 클릭된 마커가 실행되는 함수
   function detail(stnum) {
        var res = confirm('상세보기 페이지로 넘어 가시겠습니까?');
        if (res == false) return;
        location.href = "storeDetail?stnum="+stnum;
    }
   
</script>
<div class="content">

   <style>
#list {
   color: black;
   border: solid 1px;
   text-align: center;
   width: 1000px;
   margin: auto;
}

#store_list th {
   text-align: center;
}
#sido0{
    position: relative;
}

</style>
   <div id="list">

      <fieldset>
         <legend>마카롱 가게 리스트</legend>

         <div class="row" id="sido_form">
            <div class="col-md-6">
               <div  id="map" style="width: 350px; height: 300px; margin: 70px">
               </div>
            </div>

            <div class="col-md-6" id="search_menu" style="height: 386px;">
                  <div class="mRight" style="margin-top: 100px;">
                     <h1> 매장찾기 </h1>
                     <br>
                     <hr>
                     <br>
                     <div class="selectArea" style="text-align: center; margin-top: 20px;"> <!--  durl  -->
                        <label style="float: left; margin-top: 5px;">지역선택</label> 
                        <div class="col-xs-3">
                        <!--  ajax 처리되는 select 태그 -->
                        <select name="sido" id="sido" onchange="sChangeCategory1()"  class="form-control" style="width: auto;">
                           <option value="선택">시/도 선택</option>
                           <option value="서울">서울</option>
                           <option value="부산">부산</option>
                           <option value="대구">대구</option>
                           <option value="인천">인천</option>
                           <option value="광주">광주</option>
                           <option value="대전">대전</option>
                           <option value="울산">울산</option>
                           <option value="강원">강원</option>
                           <option value="경기">경기</option>
                           <option value="제주">제주</option>
                           <option value="충남">충남</option>
                           <option value="충북">충북</option>
                           <option value="세종">세종</option>
                        </select> 
                        </div>
                        <div class="col-xs-3">
                        <select name="gugun" id="gugun" class="form-control" style="width: auto;">
                           <option value="선택">구/군 선택</option>
                        </select>
                        </div> 
                        <div class="col-xs-3">
                        <input type="text" name="search" id="search" class="form-control" >
                        <!--  상세 주소로 검색 -->
                        <a href="javascript:search('storelistController');" class="btn_07">매장찾기</a>   
                        </div> 
                     </div>
                  </div>
               </div>   
         </div>
         <br>
         <br>
         <div class="row">
         <!--  번호, 이름, 주소로 검색하는 폼 -->
                     <form method="post" action="storelistController"
                        class="form-inline">
                        <input type="hidden" name="page" value="${param.page }">
                        <select name="searchType" class="form-control">
                           <option value="1">번호</option>
                           <option value="2">이름</option>
                           <option value="3">주소</option>
                        </select> &nbsp; <input class="form-control" name="searchValue"
                           placeholder="번호 or 이름 or 주소를 입력" required> <input
                           class="btn btn-outline-dark" type="submit" value="검색">
                     </form>
            <br>
         <br>
      <br>
         </div>

         <table class="table table-hover table-bordered" id="store_list">
            <thead>
               <tr>
                  <th>번호</th>
                  <th>가게 이름</th>
                  <th>주소</th>
                  <th>전화 번호</th>
                  <th>이미지</th>
               </tr>
            </thead>
            <tbody>
            <!--  검색 결과가 없을시 -->
               <c:if test="${empty list }">
                  <script>
                     alert("검색 값이 없슴니당");
                     location.href = "storelistController?page=1";
                  </script>
               </c:if>
               <!--  검색 결과가 있다면 출력되는 리스트 -->
               <c:forEach var="e" items="${list }">
                  <tr>
                     <td><a href="storeDetail?stnum=${e.stnum }">${e.stnum }</a></td>
                     <td><a href="storeDetail?stnum=${e.stnum }">${e.stsbname }</a></td>
                     <td><a href="storeDetail?stnum=${e.stnum }">${e.staddr1 } ${e.staddr2 }</a></td>
                     <td><a href="storeDetail?stnum=${e.stnum }">${e.stphone }</a></td>
                     <td><a href="storeDetail?stnum=${e.stnum }"><img
                           src="${pageContext.request.contextPath }/resources/imgfile/${e.stimg }"
                           style="width: 100px; border: 0px"></a></td>
                  </tr>
               </c:forEach>
               
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="5">
                     <%@include file="page.jsp"%>
                  </td>
               </tr>
            </tfoot>
         </table>
      </fieldset>
   </div>
   <br> <br> <br>
</div>
<%@include file="footer.jsp"%>