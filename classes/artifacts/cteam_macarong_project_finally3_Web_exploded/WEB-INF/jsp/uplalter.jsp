<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@include file="header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br>
<br>
<br>
<br>
<br>
<br>
<form method="post" action="update" autocomplete="off"
	enctype="multipart/form-data">
	<fieldset
		style="width: 500px; height: 500; margin: auto; color: black;">
		<table>
			<tr>
				<th style="width: 100px">음식 이름</th>
				<th style="width: 100px">음식가격</th>
				<th style="width: 100px">이미지</th>
				<th style="width: 100px">음식 상세 정보</th>
				<th style="width: 100px">음식 분류</th>
			</tr>
			<input type="hidden" name="foodnum" value="${menu.foodnum}">
			<tr>
				<td><input type="text" name="foodname"
					value="${menu.foodname }"></td>
				<td><input type="text" name="foodpay" value="${menu.foodpay }"></td>
				<td><input type="file" name="upimg" id="upimg"
					value="${menu.foodimg }"></td>


				<td><textarea name="fooddetail" id="fooddetail" cols="40"
						rows="8">${menu.fooddetail}</textarea></td>
				<td><select name="category" id="category">
						<option value="${menu.category}">${menu.category}</option>

						<option value="마카롱">마카롱</option>
						<option value="음료">음료</option>
						<option value="기타">기타</option>

				</select></td>


				<td><input type="submit" value="등록"></td>
			</tr>
		</table>
	</fieldset>
</form>
<%@include file="footer.jsp"%>