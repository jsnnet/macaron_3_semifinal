package kr.co.macaronshop.mvc.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

@Component
@Aspect
public class AroundSessionChkAdvice {
   // 회원의 로그인 유무를 확인하는 aop
   @Around("execution(* kr.co.macaronshop.mvc.controller.MacaronMember*.m*(..))")
   public Object MemloginCheck(ProceedingJoinPoint jp) throws Throwable {
      
      Object ob[] = jp.getArgs();
      // 1. 단순 로그인 페이지 폼으로 이동
      if(jp.getSignature().getName().equals("mloginForm")) {
         return jp.proceed();
      }
      
      for(Object aa : ob) {
         
         if (aa instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) aa;
            HttpSession session = request.getSession();
      
            if (session.getAttribute("uid") == null) {
               // 2. 로그인을 하지 않은 사용자가 로그인을 시도할 때.
               if(jp.getSignature().getName().equals("mloginfProcess")) {
                  return jp.proceed();
               }
               // 3. 로그인을 하지않은 사용자가 어떠한 기능을 사용하려고 시도할때
               // 로그인 폼으로 이동하고, 어떠한 요청을 시도했는지에 대한 값을 로그인 폼으로 알려준다.
               ModelAndView mav = new ModelAndView();
               mav.addObject("stnum", request.getParameter("stnum"));
               mav.addObject("error", "error");
               mav.addObject("method",jp.getSignature().getName());
               mav.setViewName("loginForm_user");
               return mav;
            // 4. 세션에 정보가 있는 즉 로그인을 성공한 사용자가 어떠한 기능을 실행 하려고 했을때. 
            }else if(session.getAttribute("uid") != null) {
               return jp.proceed();
            }
         }
      }
      return null;
   }
   // 관리자의 로그인 유무를 확인하는 aop
   @Around("execution(* kr.co.macaronshop.mvc.controller.MacaronAdmin*.a*(..))")
   public Object AdminloginCheck(ProceedingJoinPoint jp) throws Throwable {
      Object ob[] = jp.getArgs();
      
      // 1. 단순 로그인 페이지 폼으로 이동
      if(jp.getSignature().getName().equals("stloginForm")) {
         return jp.proceed();
      }
      
      for(Object aa : ob) {
         
         if (aa instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) aa;
            HttpSession session = request.getSession();
            
            if (session.getAttribute("stid") == null) {
               // 2. 로그인을 하지 않은 관리자가 로그인을 시도할 때.
               if(jp.getSignature().getName().equals("stloginfProcess")) {
                   return jp.proceed(); 
               }
               // 3. 로그인을 하지않은 관리자가 어떠한 기능을 사용하려고 시도할때
               // 로그인 폼으로 이동하고, 어떠한 요청을 시도했는지에 대한 값을 로그인 폼으로 알려준다.
               ModelAndView mav = new ModelAndView();
               mav.addObject("stnum", request.getParameter("stnum"));
               mav.addObject("error", "error");
               mav.addObject("method",jp.getSignature().getName());
               mav.setViewName("stloginForm");
               return mav;
            // 4. 세션에 정보가 있는 즉 로그인을 성공한 관리자가 어떠한 기능을 실행 하려고 했을때. 
            }else if(session.getAttribute("stid") != null) {
               return jp.proceed();
            }
         }
      }
      return null;
   }
}
