package kr.co.macaronshop.mvc.aop;

import kr.co.macaronshop.mvc.dao.AopInterestDao;
import kr.co.macaronshop.mvc.dto.BasketVO;
import kr.co.macaronshop.mvc.dto.Mem_Interest_LogVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
@Aspect
public class InterestAroundAdvice {

	@Autowired
	private AopInterestDao dao;
	
	// 회원의 선호도 조사를 위한 aroundAop
	@Around("execution (* kr.co.macaronshop.mvc.controller.MacaronMemberI*.i*(..))")
	public Object interestAdvice(ProceedingJoinPoint jp) throws Throwable {
		Object ob[] = jp.getArgs();
		Mem_Interest_LogVO vo = new Mem_Interest_LogVO();
		// 특정 회원이 구매, 장바구니에 등록하려 할때 값들을 뽑아오는 전처리
		for(int i = 0; i < ob.length; i ++) {
			if(ob[i] instanceof BasketVO) {
				BasketVO bvo = (BasketVO) ob[i];
				vo.setFoodnum(bvo.getFoodnum());
				vo.setMemnum(bvo.getMemnum());
				vo.setStnum(bvo.getStnum());
			}else if(ob[i] instanceof PaymentVO) {
				PaymentVO pvo = (PaymentVO) ob[i];
				vo.setMemnum(pvo.getMemnum());
				vo.setStnum(pvo.getStnum());
			}else if(ob[i] instanceof String) {
				String foodnum = (String) ob[i];
				vo.setFoodnum(Integer.parseInt(foodnum));
			}else if(ob[i] instanceof HttpServletRequest) {
				HttpServletRequest req = (HttpServletRequest) ob[i];
				HttpSession session = req.getSession();
				vo.setMemnum((int) session.getAttribute("unum"));
			}
		}
		ModelAndView mav = (ModelAndView) jp.proceed();
		
		// 회원의 로그 기록을 확인하여 
		Mem_Interest_LogVO mvo = dao.memInterestLog(vo);
		// 한번도 구매한 적이 없는 가게, 가게의 메뉴라면 
		if(mvo == null) {
			dao.firstMemIntLog(vo);
			return mav;
		// 구매한 적이 있다면 update 해주세요
		}else{
			dao.updateMemIntLog(mvo);
			return mav;
		}
	}
}
