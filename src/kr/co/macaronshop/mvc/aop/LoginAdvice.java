package kr.co.macaronshop.mvc.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MemberDao;
import kr.co.macaronshop.mvc.dto.LoginLoggerDTO;

@Component
@Aspect
public class LoginAdvice {
	@Autowired
	private MemberDao membersDaoInter;
	
	// 회원의 로그인 기록을 저장하는 aop
	@Around("execution(* kr.co.macaronshop.mvc.controller.MacaronMemberLogin*.mloginf*(..))")
	public ModelAndView loginLogger(ProceedingJoinPoint jp) {
		    Object[] fd = jp.getArgs();
			ModelAndView rpath=null;
			String methodName = jp.getSignature().getName();
			if(methodName.equals("mloginfProcess")) {
				try {
					LoginLoggerDTO vo = new LoginLoggerDTO();
					rpath = (ModelAndView) jp.proceed();
					if(fd[0] instanceof HttpSession &&
							fd[1] instanceof HttpServletRequest) {
					HttpSession session = (HttpSession) fd[0];
					HttpServletRequest request = (HttpServletRequest) fd[1];
					String uid = (String) session.getAttribute("uid");	
						if(uid == null) { 
						}else {
							vo.setIdn(uid);
							vo.setStatus("login");
							vo.setReip(request.getRemoteAddr());
							membersDaoInter.addLoginLogging(vo);
						}
					
					}
				} catch (Throwable e) {
					e.printStackTrace();
				}
				
			}else if(methodName.equals("mloginfoutProcess")) {
				try {
					LoginLoggerDTO vo = new LoginLoggerDTO();
					if(fd[0] instanceof HttpSession 
							&& fd[1] instanceof HttpServletRequest) {
						HttpSession session = (HttpSession) fd[0];
						HttpServletRequest request = (HttpServletRequest) fd[1];
						String uid = (String) session.getAttribute("uid");	
							if(uid == null) { 
							}else {
								vo.setIdn(uid);
								vo.setStatus("logout");
								vo.setReip(request.getRemoteAddr());
								membersDaoInter.addLoginLogging(vo);
							}
						}
					rpath = (ModelAndView) jp.proceed();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			return rpath;			
		}
}
