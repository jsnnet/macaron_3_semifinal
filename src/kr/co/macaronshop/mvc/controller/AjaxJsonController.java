package kr.co.macaronshop.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import kr.co.macaronshop.mvc.dao.AjaxDao;
import kr.co.macaronshop.mvc.dao.PaymentDao;
import kr.co.macaronshop.mvc.dto.Mem_Interest_LogVO;
import kr.co.macaronshop.mvc.dto.StoreVO;
import kr.co.macaronshop.mvc.dto.TboardVO;


@RestController
@RequestMapping(value = "/store")
public class AjaxJsonController {
   // ajax 처리결과를 json 파일로 만들기 위한 controller
   @Autowired
   private AjaxDao ajaxdao;
   @Autowired
   private PaymentDao paymentDao;
   	  // 더치 페이 하는 도중에 결제 취소 
   	  @RequestMapping(value = "/dutch_delete")
      @ResponseBody
      public ResponseEntity<String> dutch_delete(int pay_num) {
         paymentDao.setdutch_delete(pay_num);
         String list = "dutch_delete_OK!!!!!!";
         return new ResponseEntity<>(list, HttpStatus.OK);
      }
      
   	  // 결제 
      @RequestMapping(value = "/pay")
      @ResponseBody
      public ResponseEntity<String> pay(int pay_num) {
         paymentDao.setUpdatepay(pay_num);
         String list = "";
         return new ResponseEntity<>(list, HttpStatus.OK);
      }
   
   // 가장 평점 높은 가게리스트 출력
   @RequestMapping(value = "/popular")
   @ResponseBody
   public ResponseEntity<List<TboardVO>> popularStore() {
         List<TboardVO> list = new ArrayList<>();
         list.addAll(ajaxdao.popularStore());
            return new ResponseEntity<>(list , HttpStatus.OK);
      }
   
   // 특정 회원의 선호도 높은 가게 출력 
   @RequestMapping(value = "/interestmem")
   @ResponseBody
   public ResponseEntity<List<Mem_Interest_LogVO>> interestMem(int unum) {
         List<Mem_Interest_LogVO> list = new ArrayList<>();
         list.addAll(ajaxdao.memInterLog(unum));
            return new ResponseEntity<>(list , HttpStatus.OK);
      }
   
   // 특정 가게에 등록 되어 있는 메뉴 출력
   @RequestMapping(value = "/menu")
   @ResponseBody
   public ResponseEntity<List<StoreVO>> respList2(int stnum) {
         List<StoreVO> list = new ArrayList<>();
         list.addAll(ajaxdao.store_menu(stnum));
            return new ResponseEntity<>(list , HttpStatus.OK);
      }
   
   // 가게를 주소별로 검색시 시,도에 따른 구,군 출력
   @RequestMapping(value = "/sido")
   @ResponseBody
   public ResponseEntity<List<String>> sidoList(String sido){
      List<String> list = new ArrayList<String>();
      switch (sido) {
      case "서울":
               list.add("강남구");
               list.add("강동구");
               list.add("강북구");
               list.add("강서구");
               list.add("관악구");
               list.add("구로구");
               list.add("금천구");
               list.add("노원구");
               list.add("도봉구");
               list.add("동대문구");
               list.add("동작구");
               list.add("마포구");
               list.add("서대문구");
               list.add("서초구");
               list.add("성동구");
               list.add("영등포");
               list.add("은평구");
               list.add("종로구");
         break;
      case "부산":
               list.add("강서구");
               list.add("남구");
               list.add("동래");
               list.add("동구");
               list.add("서구");
               list.add("해운대구");
               list.add("중구");
               list.add("수영구");
         break;
      case "대구":
               list.add("남구");
               list.add("달서구");
               list.add("동구");
               list.add("북구");
               list.add("서구");
               list.add("수성구");
               list.add("중구");
               list.add("달성군");
         break;
      case "인천":
         list.add("계양구");
         list.add("남구");
         list.add("남동구");
         list.add("부평구");
         list.add("서구");
         list.add("연수구");
         list.add("중구");
         list.add("옹진군");
         break;
      case "광주":
         list.add("광산구");
         list.add("남구");
         list.add("동구");
         list.add("북구");
         list.add("서구");
         break;
      case "대전":
         list.add("대덕구");
         list.add("동구");
         list.add("서구");
         list.add("유성구");
         list.add("중구");
         break;
      case "울산":
         list.add("남구");
         list.add("동구");
         list.add("북구");
         list.add("중구");
         list.add("울주군");
         break;
      case "강원":
         list.add("강릉시");
         list.add("동해시");
         list.add("삼척시");
         list.add("속초시");
         list.add("원주시");
         list.add("춘천시");
         list.add("태백시");
         list.add("고성군");
         list.add("양구군");
         list.add("영월군");
         list.add("인제군");
         list.add("철원군");
         list.add("평창군");
         list.add("화천군");
         list.add("횡성군");
         break;
      case "경기":
         list.add("고양시");
         list.add("과천시");
         list.add("광명시");
         list.add("광주시");
         list.add("구리시");
         list.add("군포시");
         list.add("김포시");
         list.add("남양주시");
         list.add("성남시");
         list.add("수원시");
         list.add("시흥시");
         list.add("안산시");
         list.add("안성시");
         list.add("안양시");
         list.add("오산시");
         list.add("용인시");
         list.add("의왕시");
         list.add("평택시");
         list.add("화성시");
         list.add("가평시");
         break;
      case "제주":
         list.add("제주시");
         list.add("서귀포시");
         break;
      case "충남":
         list.add("계룡시");
         list.add("공주시");
         list.add("논산시");
         list.add("보령시");
         list.add("서산시");
         list.add("천안시");
         list.add("연기군");
         list.add("태안군");
         list.add("홍성군");
         break;
      case "충북":
         list.add("제천시");
         list.add("청주시");
         list.add("충주시");
         list.add("단양군");
         list.add("음성군");
         break;
      case "세종":
         break;
      default:
         break;
      }
      return new ResponseEntity<List<String>>(list, HttpStatus.OK);
   }
}