package kr.co.macaronshop.mvc.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MenuDao;
import kr.co.macaronshop.mvc.dao.StoreDao;
import kr.co.macaronshop.mvc.dto.MenuVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;
import kr.co.macaronshop.mvc.dto.StoreVO;
@Controller
public class MacaronAdminController {
	
	@Autowired
	private StoreDao storeDaoInter;
	@Autowired
	private MenuDao menuDao;

	// 특정 가게의 마이페이지 
	@RequestMapping(value = "/admypage") 
	public ModelAndView admypage(HttpServletRequest request ,HttpSession session) {
		int stnum = (int)session.getAttribute("stnum");
		StoreVO svo = storeDaoInter.getStoreinfor(stnum);
		
		List<PaymentVO> paylist = storeDaoInter.getOrderList(stnum);
		ModelAndView mav = new ModelAndView("admypage");
		mav.addObject("paylist",paylist);
		mav.addObject("svo",svo);
		return mav;
	}
	
	// 특정 가게의 수정 폼
	@GetMapping("/stmyupdate")
	public String stupdate() {
		return "stmyupdate";
	}
	
	// 특정 가게 정보 수정
	@RequestMapping("/stmyup")
	public String stmyupdate(HttpSession session, StoreVO vo) {
		storeDaoInter.update(vo);
		session.invalidate();
		return "up";
	}
	
	// 가게의 메뉴 등록 
	@RequestMapping(value = "/upsave", method = RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request, @ModelAttribute("vo") MenuVO vo, HttpSession session) {
			String path = session.getServletContext().getRealPath("/resources/imgfile/");
			StringBuffer paths = new StringBuffer();
			paths.append(path);
			paths.append(vo.getUpimg().getOriginalFilename());
			File f = new File(paths.toString());

			try {
				vo.getUpimg().transferTo(f);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
			vo.setFoodimg(vo.getUpimg().getOriginalFilename());
			menuDao.menuInsert(vo);
			
		ModelAndView mav = new ModelAndView("uplist");
		int stnum = (int) session.getAttribute("stnum");
		List<StoreVO> list = menuDao.getlist(stnum);
		mav.addObject("list", list);
		return mav;
	}

	// mypage
	@RequestMapping("stmypage")
	public ModelAndView mypage(HttpServletRequest request, HttpSession session) {
		int stnum = (int) session.getAttribute("stnum");		
		ModelAndView mav = new ModelAndView("uplist");
		List<StoreVO> list = menuDao.getlist(stnum);
		mav.addObject("list", list);
		return mav;
	}

	//delete
	@RequestMapping("delete")
	public ModelAndView delete(HttpServletRequest request, int foodnum,HttpSession session) {
		int stnum = (int) session.getAttribute("stnum");	
		menuDao.getdelet(foodnum);
		ModelAndView mav = new ModelAndView("uplist");
		List<StoreVO> list = menuDao.getlist(stnum);
		mav.addObject("list", list);
		return mav;
		
	}
	//alter
	@RequestMapping("alter")
	public ModelAndView alter(HttpServletRequest request, int foodnum,HttpSession session) {
		
		ModelAndView mav = new ModelAndView("uplalter");
		MenuVO menu = menuDao.getMenu(foodnum);
		mav.addObject("menu", menu);
		return mav;
	}
	
	//update
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request, @ModelAttribute("vo") MenuVO vo, HttpSession session) {

		String path = session.getServletContext().getRealPath("/resources/imgfile/");
		StringBuffer paths = new StringBuffer();
		paths.append(path);
		paths.append(vo.getUpimg().getOriginalFilename());
		File f = new File(paths.toString());

		try {
			vo.getUpimg().transferTo(f);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		vo.setFoodimg(vo.getUpimg().getOriginalFilename());
		
		menuDao.getmenuupdate(vo);
		
		ModelAndView mav = new ModelAndView("uplist");
		int stnum = (int) session.getAttribute("stnum");
		List<StoreVO> list = menuDao.getlist(stnum);
		mav.addObject("list", list);
		return mav;
	}
}
