package kr.co.macaronshop.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.StoreDao;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Controller
public class MacaronAdminLoginCheckController {

	
		@Autowired
		private StoreDao StoreDaoInter;

		// 가게 로그인 페이지 폼 이동 
		@RequestMapping(value = "/stloginForm")
		public String stloginForm(HttpServletRequest request) {
			return "stloginForm";
		}

		// 가게 로그인 프로세스 
		@PostMapping("/stloginProcess")
		public ModelAndView stloginfProcess(HttpSession session,
				HttpServletRequest request, StoreVO vo,  String method,
				@RequestHeader("User-Agent") String userAgent) {
			StoreVO map = StoreDaoInter.loginCheck(vo);
			ModelAndView mav = new ModelAndView();
			if(map == null) {
				    mav.setViewName("error");	  
			}else  { 
					session.setAttribute("stnum", map.getStnum());
					session.setAttribute("stid", map.getStid());
					session.setAttribute("stname", map.getStname());
					session.setAttribute("adminid", map.getStid());
					if(method.equals("admypage")) {
						mav.setViewName("redirect:/admypage");
						return mav;
					}
					mav.setViewName("index");
			}
			return mav;
		}
		// 가게 로그아웃 프로세스
		@GetMapping("/stlogout")
		public ModelAndView stloginfoutProcess(HttpServletRequest request, HttpSession session) {
			session.removeAttribute("stnum");
			session.removeAttribute("stname");
			session.removeAttribute("stid"); 
			ModelAndView mav = new ModelAndView();
			mav.setViewName("stlogout");
			return mav;
		}
}
