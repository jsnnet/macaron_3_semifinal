package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MemberDao;
import kr.co.macaronshop.mvc.dao.StoreDao;
import kr.co.macaronshop.mvc.dto.LoginLoggerDTO;
import kr.co.macaronshop.mvc.dto.MemberVO;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Controller
public class MacaronJoinController {
	// ---------------------고객 회원가입------------------
	@Autowired
	private MemberDao memberDaoInter;

	@RequestMapping(value = "member")
	public ModelAndView joinForm(HttpServletRequest request) {
		if(request.isRequestedSessionIdValid()) {
			ModelAndView mav = new ModelAndView("member");
			return mav;
		}else {
			ModelAndView mav = new ModelAndView("index");
			return mav;
		}
	}
	
	@GetMapping("/memberin")
	public String memberForm() {
		return "memberin";
	}

	@GetMapping("/my_update")
	public String update() {
		return "update";
	}

	@PostMapping("/memberjoin")
	public String memberjoin(Model m, MemberVO vo) {
		memberDaoInter.addMem(vo);
		return "index";
	}

	@GetMapping("/idcheck")
	public ModelAndView idCheck(Model m, @RequestParam("memid") String memid) {
		ModelAndView mav = new ModelAndView();
		int cnt = memberDaoInter.idchk(memid);
		String msg = "가입 가능 ";
		if (cnt == 1) {
			msg = "가입 불가";
		}
		mav.addObject("cnt", cnt);
		mav.setViewName("idchk");
		return mav;
	}



	@GetMapping("/mypage")
	public String myPage(Model m, HttpSession session) {
		String id = (String) session.getAttribute("uid");
		List<LoginLoggerDTO> list = memberDaoInter.mypage(id);
		m.addAttribute("list", list);
		return "mypage";
	}

	@RequestMapping("loginCheck.do")
	public ModelAndView loginCheck(Model m, @RequestParam("memid") String memid) {
		ModelAndView mav = new ModelAndView();
		int cnt = memberDaoInter.idchk(memid);
		String msg = "가입 가능 ";
		if (cnt == 1) {
			msg = "가입 불가";
		}
		mav.addObject("cnt", cnt);
		mav.setViewName("idchk");
		return mav;
	}

	@GetMapping("/mypagein")
	public String mypagein() {
		return "mypagein";
	}

	@RequestMapping("/myupdate")
	public String update(HttpSession session, MemberVO vo) {
		memberDaoInter.update(vo);
		session.invalidate();
		return "up";
	}

	// ---------------------가게 회원가입------------------

	@Autowired
	private StoreDao storeDaoInter;

	@GetMapping("/stmemberin")
	public String stmemberForm() {
		return "stmemberin";
	}

	@PostMapping("/storejoin")
	public String storejoin(Model m, StoreVO vo) {
		storeDaoInter.addStore(vo);
		return "index";
	}

	@GetMapping("/stidchk")
	public ModelAndView idCheck1(Model m, @RequestParam("stid") String stid) {
		ModelAndView mav = new ModelAndView();
		int cnt = storeDaoInter.idChk(stid);
		String msg = "가입 가능 ";
		if (cnt == 1) {
			msg = "가입 불가";
		}
		mav.addObject("cnt", cnt);
		mav.setViewName("stidchk");
		return mav;
	}

	@GetMapping("/sstidchk")
	public ModelAndView stsbnameidCheck(Model m, @RequestParam("stsbname") String stsbname) {
		ModelAndView mav = new ModelAndView();
		int cnt = storeDaoInter.idChk(stsbname);
		String msg = "가입 가능 ";
		if (cnt == 1) {
			msg = "가입 불가";
		}
		mav.addObject("msg", msg);
		mav.addObject("cnt", cnt);
		mav.setViewName("sstidchk");
		return mav;
	}

	@RequestMapping("/sterror")
	public String error() {
		return "sterror";
	}

}

