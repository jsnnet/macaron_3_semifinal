
package kr.co.macaronshop.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.BasketDao;
import kr.co.macaronshop.mvc.dto.BasketListDTO;
import kr.co.macaronshop.mvc.dto.BasketVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Controller
public class MacaronMemberIBasketController {
	
	@Autowired
	private BasketDao basketDao;
	
	int dog;

	// 장바구니 추가
	@RequestMapping(value = "/intoBasket", method = RequestMethod.POST)
	public ModelAndView intoBasket(@ModelAttribute("vo") BasketVO vo, HttpSession session, int dok
			, HttpServletRequest request) {
		int memnum = (int) session.getAttribute("unum");
		vo.setBasketpay(vo.getBasketpay()*vo.getFoodtotal());
		vo.setMemnum(memnum);
		basketDao.intoCart(vo);
		ModelAndView mav = new ModelAndView("redirect:/listBasket2");
		dog = dok;
		return mav;
	}
	
	// 장바구니 보여주기 수정 (select)
		@RequestMapping(value = "/listBasket2")
		public ModelAndView listBasket2(HttpSession session) {
			int memnum = (int) session.getAttribute("unum"); // 세션에 있던 회원번호(unum)을 받아 이용하기
			PaymentVO pvo = new PaymentVO();
			pvo.setDok(dog);
			ModelAndView mav = new ModelAndView("basketlist2");
			mav.addObject("dog", dog);
			List<BasketListDTO> dtolist = basketDao.listBasket2(memnum);
			BasketListDTO tp = basketDao.totalPrice(memnum);// 장바구니에 담긴 모든 상품에 대한 주문가격의 합계
			mav.addObject("tp", tp);
			mav.addObject("dtolist", dtolist);
			return mav;
		}
	

		// 개별 삭제
		@GetMapping(value = "/deleteselected")
		public String delCart(int basketnum) {
			basketDao.delCart(basketnum);
			return "redirect:/listBasket2";
		}
	
		// 장바구니에서 구매 버튼 클릭 그리고 구매된 항목들은 장바구니에서 제거
		@RequestMapping(value = "/clickPay", method = RequestMethod.POST)
		public ModelAndView clickPay(@RequestParam(value = "chk[]") int[] chbox, HttpSession sesstion, boolean check) {
			ModelAndView mav = new ModelAndView("redirect:/storelistController?page=1");
			int memnum = (int) sesstion.getAttribute("unum");
				List<BasketVO> list = new ArrayList<BasketVO> ();
				BasketVO ppvo = new BasketVO();
		      for(int e  : chbox) {
		    	  ppvo =basketDao.cartPay(e);
		    	  basketDao.insertPay(ppvo);
		    	  basketDao.delCart(e);
		      }
		      return mav;
		}
}

