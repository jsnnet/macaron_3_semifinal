package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MyPageDao;
import kr.co.macaronshop.mvc.dao.PaymentDao;
import kr.co.macaronshop.mvc.dto.Dutch_PayVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Controller
public class MacaronMemberIPayController {
	@Autowired
	private PaymentDao paymentDao;
	@Autowired
	private MyPageDao myPageDao;

	// 결제 폼 이동
	@RequestMapping("pay")
	public ModelAndView ipay(PaymentVO pvo, String foodnum) {
		ModelAndView mav = new ModelAndView("pay");
		mav.addObject("msg", "결제페이지 입니다.");
		mav.addObject("pvo", pvo);
		mav.addObject("foodnum", foodnum);
		return mav;
	}

	// 결제
	@RequestMapping("payment")
	public String paypay(PaymentVO pvo, HttpSession session) {
		pvo.setMemnum((int) session.getAttribute("unum"));
		paymentDao.getInsert(pvo);
		return "hi";
	}

	// 더치페이를 하기 위하여 진행중인 더치 페이 확인, 친구 유무도 확인하는 메서드 
	@RequestMapping("dutch_pay")
	public ModelAndView duch_pay(PaymentVO pvo, HttpSession session) {
		int memnum = (int) session.getAttribute("unum");
		String memid = (String) session.getAttribute("uid");
		int pro_status = paymentDao.getProStatus(memnum);
		int count_friend = paymentDao.getCountFriend(memid);
		if (count_friend == 0) {
			ModelAndView mav = new ModelAndView("pay");
			mav.addObject("msg", "추가한 친구가 없습니다.");
			mav.addObject("pvo", pvo);
			return mav;
		} else if (pro_status == 1) {
			ModelAndView mav = new ModelAndView("pay");
			mav.addObject("msg", "이미진행중인 더치페이가 있습니다!");
			mav.addObject("pvo", pvo);
			return mav;
		} else {

			// 더치 페이 결제 목록 
			ModelAndView mav = new ModelAndView("dutch_pay");
			
			List<String> friend_list = myPageDao.getfriendList(memid);
			mav.addObject("friend_list", friend_list);
			mav.addObject("pvo", pvo);
			return mav;
		}

	}

	// 더치 페이 결제 진행 사항 
	@RequestMapping("dutch_pro")
	public String duch_pro(PaymentVO pvo, HttpSession session, Dutch_PayVO dvo) {
		if(dvo.getPay().equals("")) {
			dvo.setPay1(0);
		}else {
			dvo.setPay1(Integer.parseInt(dvo.getPay()));
		}
		pvo.setMemnum((int) session.getAttribute("unum"));
		dvo.setMemid1((String) session.getAttribute("uid"));
		
		paymentDao.setPaymentDutchInsert(pvo);
		
		dvo.setPay_num(paymentDao.getPayNum(pvo.getMemnum()));
		paymentDao.setDutchInsert(dvo);
		
		return "redirect:myPage";
	}

	// 더치페이에 대한 나머지 금액을 결제 하는 메서드
	@RequestMapping("dutch_update")
	public String dutch_update(Dutch_PayVO dvo, HttpSession session) {

		if (dvo.getPay().equals("")) {
			return "redirect:dutch_status";
		} else {
			//int pay1 = Integer.parseInt(dvo.getPay());
			String memid = (String) session.getAttribute("uid");
			dvo.setPay1(Integer.parseInt(dvo.getPay()));
			dvo.setMemid(memid);
			paymentDao.setDutchUpdate(dvo);
			return "redirect:dutch_status";
		}
	}
	
}
