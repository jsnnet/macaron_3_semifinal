package kr.co.macaronshop.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MemberDao;
import kr.co.macaronshop.mvc.dao.StoreDao;
import kr.co.macaronshop.mvc.dto.MemberVO;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Controller
public class MacaronMemberLoginCheckController {
	//----------------------맴버 로그인------------------------------------
	@Autowired
	private MemberDao membersDaoInter;

	@RequestMapping(value = "/loginForm")
	public String mloginForm(HttpServletRequest request) {
		return "loginForm_user";
	}
	//spring인경우 HttpSession 주입,
	// @RequestHeader 요청시 넘어오는 해더값을 받을 수 있는 어노테이션
	@PostMapping("/loginProcess_user")
	public ModelAndView mloginfProcess(HttpSession session,
			HttpServletRequest request, MemberVO vo, String method, String stnum,
			@RequestHeader("User-Agent") String userAgent) {
		MemberVO map = membersDaoInter.loginCheck(vo);
		ModelAndView mav = new ModelAndView();
		if(map == null) {
			mav.setViewName("error");
			
		}else  { // 로그인 성공
				session.setAttribute("unum", map.getMemnum());
				session.setAttribute("uname", map.getMemname());
				session.setAttribute("uid", map.getMemid());
				// 로그인을 하지 않고, 특정 메뉴를 눌렀을시에 넘어왔던 타겟 메서드의 이름을
				// 로그인 성공시 사용하여 값을 넘겨준다. 
				if(method.equals("storelistController")) {
					mav.setViewName("redirect:/storelistController?page=1");
					return mav;
				}else if(method.equals("mfriendList")) {
					mav.setViewName("redirect:/myPage");
					return mav;
				}else if(method.equals("mstoreDetail")) {
					mav.setViewName("redirect:/storeDetail?stnum="+stnum);
					return mav;
				}else if(method.equals("mlistBasket2")) {
					mav.setViewName("redirect:/listBasket2");
					return mav;
				}else if(method.equals("mBoardlist")) {
					mav.setViewName("redirect:/boardUplist?page=1");
					return mav;
				}
				mav.setViewName("index");
		}
		return mav;
	}
	// 회원 로그아웃
	@GetMapping("/logout")
	public ModelAndView mloginfoutProcess(HttpSession session,
			HttpServletRequest request) {
		session.removeAttribute("umum");
		session.removeAttribute("uname");
		session.removeAttribute("uid"); // 로그인한 사용자의 세션 삭제
		ModelAndView mav = new ModelAndView();
		mav.setViewName("logout");
		return mav;
	}
	@RequestMapping("/error") 
	public String error() {
		return "error";
	}

}
