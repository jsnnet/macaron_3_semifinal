package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.MemberDao;
import kr.co.macaronshop.mvc.dao.MyPageDao;
import kr.co.macaronshop.mvc.dao.PaymentDao;
import kr.co.macaronshop.mvc.dto.Dutch_PayVO;
import kr.co.macaronshop.mvc.dto.FriendVO;
import kr.co.macaronshop.mvc.dto.LoginLoggerDTO;
import kr.co.macaronshop.mvc.dto.MemberVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Controller
public class MacaronMemberMyPageController {

	@Autowired
	private MyPageDao myPageDao;
	@Autowired
	private PaymentDao paymentDao;
	@Autowired
	private MemberDao memberDaoInter;
	
	// 회원의 마이페이지 
	@RequestMapping("myPage")
	public ModelAndView mfriendList(HttpSession session, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("mypage");
		String memid = (String) session.getAttribute("uid");
		MemberVO mvo = myPageDao.getMeminfor(memid);
		mav.addObject("mvo",mvo);
		
		List<String> friend_list = myPageDao.getfriendList(memid);
		mav.addObject("friend_list", friend_list);

		List<PaymentVO> pay_list = paymentDao.getpaylist(memid);
		mav.addObject("pay_list", pay_list);
		int cnt = myPageDao.getWaitCnt(memid);
		int cnt_apply = myPageDao.getWaitCnt_apply(memid);
		
		int cnt_dutch = myPageDao.getWaitCnt_Dutch(memid);
		
		
		mav.addObject("cnt", cnt);
		mav.addObject("cnt_dutch", cnt_dutch);
		mav.addObject("cnt_apply", cnt_apply);
		return mav;
	}

	@RequestMapping("friendplus")
	public ModelAndView friendplus(HttpSession session, String friendid) {
		ModelAndView mav = new ModelAndView();
		String memid = (String) session.getAttribute("uid");
		FriendVO fvo = new FriendVO();
		FriendVO fvo2 = new FriendVO();
		fvo.setMemid(memid);
		fvo.setFriendid(friendid);
		try {
			// 친구 상태일때 
			fvo2 = myPageDao.getChk(fvo);
			//waiting -> 0 : 내가 신청한 상태 
			if (fvo2.getWaiting() == 0) { 
				mav.addObject("msg", "친구 대기 상태 입니다.");
			} else {
			//waiting -> 1 : 친구 수락 상태 
				mav.addObject("msg", "이미 친구로 등록된 사용자 입니다.");
			}
		//친구가 안 되어 있거나 아이디가 없을 때 
		} catch (NullPointerException e) {
			try {
				//회원이며 친구 상태가 아닐때 
				myPageDao.getAddFriend(fvo);
				mav.addObject("msg", "친구 추가 되었습니다.");
			} catch (Exception e2) {
				//회원이 아닐때 
				mav.addObject("msg", "아이디가 없습니다.");
			}
		}
		mav.setViewName("friendWait_Apply_plus");

		return mav;
	}

	// friendWait -친구 대기 상태.
	@RequestMapping("friendWait")
	public ModelAndView friendWait(HttpSession session) {
		ModelAndView mav = new ModelAndView("friendWait_Apply_plus");
		String memid = (String) session.getAttribute("uid");
		List<String> waitList = myPageDao.getFriendWait(memid);
		mav.addObject("waitList", waitList);
		return mav;
	}

	@RequestMapping("friendaccess")
	public ModelAndView friendaccess( HttpSession session,String friendid) {
		ModelAndView mav = new ModelAndView("redirect:friendWait");
		String memid = (String) session.getAttribute("uid");
		FriendVO fvo = new FriendVO();
		fvo.setMemid(memid);
		fvo.setFriendid(friendid);
		myPageDao.getFriendaccess(fvo);
		return mav;
	}

	@RequestMapping("frienddeny")
	public ModelAndView frienddeny(HttpSession session, String friendid) {
		ModelAndView mav = new ModelAndView("redirect:friendWait");
		String memid = (String) session.getAttribute("uid");
		FriendVO fvo = new FriendVO();
		fvo.setMemid(memid);
		fvo.setFriendid(friendid);
		myPageDao.getFrienddeny(fvo);
		return mav;
	}
	

	// friendapply -
	@RequestMapping("friendapply")
	public ModelAndView friendapply(HttpSession session) {
		ModelAndView mav = new ModelAndView("friendWait_Apply_plus");
		String memid = (String) session.getAttribute("uid");
		List<String> applyList = myPageDao.getFriendApplyList(memid);
		mav.addObject("applyList", applyList);
		return mav;
	}
	
	//dutch_status 
	@RequestMapping("dutch_status")
	public ModelAndView dutch_status(HttpSession session) {
		ModelAndView mav = new ModelAndView("dutch_status");
		String memid = (String) session.getAttribute("uid");

		List<Dutch_PayVO> dutchlist = myPageDao.getDutchlist(memid);
		
		mav.addObject("dutchlist",dutchlist);
		return mav;
	}
	@GetMapping("/mylog")
	public String _myPage(Model m, HttpSession session) {
		String id = (String) session.getAttribute("uid");
		List<LoginLoggerDTO> list = memberDaoInter.mylog(id);
		m.addAttribute("list", list);
		return "mylog";
	}
	
}
