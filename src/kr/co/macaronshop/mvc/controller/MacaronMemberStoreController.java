package kr.co.macaronshop.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dao.StoreViewDao;
import kr.co.macaronshop.mvc.dto.PageVO;
import kr.co.macaronshop.mvc.dto.SearchVO;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Controller
public class MacaronMemberStoreController {
	
	@Autowired
	private StoreViewDao storeViewDao;
	
	// 가게 리스트 출력
	@RequestMapping(value = "/storelistController")
	public ModelAndView storelistController(int page, SearchVO searchVO, HttpServletRequest request) {
		
		PageVO pageInfo = new PageVO();
		int rowsPerPage = 5;			
		int pagesPerBlock = 5;		
		int currentPage = page;		
		int currentBlock = 0;			
		if(currentPage % pagesPerBlock == 0) { 
			currentBlock = currentPage / pagesPerBlock;
		}else {
			currentBlock = currentPage / pagesPerBlock+1;
		}
		int startRow = (currentPage - 1) * rowsPerPage + 1;
		int endRow = currentPage * rowsPerPage;
		SearchVO svo = new SearchVO();
		svo.setBegin(String.valueOf(startRow));
		svo.setEnd(String.valueOf(endRow));
		svo.setSearchType(searchVO.getSearchType());
		svo.setSearchValue(searchVO.getSearchValue());
		svo.setSearchValue2(searchVO.getSearchValue2());
		svo.setSearchValue3(searchVO.getSearchValue3());
		
		int totalRows = storeViewDao.getTotalCount(svo);
		int totalPages = 0;
		
		if(totalRows % rowsPerPage == 0) {
			totalPages = totalRows / rowsPerPage;
		}else {
			totalPages = totalRows / rowsPerPage + 1;
		}
		
		int totalBlocks = 0;
		if(totalPages % pagesPerBlock == 0) {
			totalBlocks = totalPages / pagesPerBlock;
		}else {
			totalBlocks = totalPages / pagesPerBlock + 1;
		}
		
		pageInfo.setCurrentPage(currentPage);
		pageInfo.setCurrentBlock(currentBlock);
		pageInfo.setRowsPerPage(rowsPerPage);
		pageInfo.setPagesPerBlock(pagesPerBlock);
		pageInfo.setStartRow(startRow);
		pageInfo.setEndRow(endRow);
		pageInfo.setTotalRows(totalRows);
		pageInfo.setTotalPages(totalPages);
		pageInfo.setTotalBlocks(totalBlocks);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("storelist");
		
		List<StoreVO> list = storeViewDao.getListSearch(svo);
		List<StoreVO> totalAddress = storeViewDao.getTotalAddress();
		
		mav.addObject("pageInfo", pageInfo);
		mav.addObject("searchType", searchVO.getSearchType());
		mav.addObject("searchValue", searchVO.getSearchValue());
		mav.addObject("searchValue2", searchVO.getSearchValue2());
		mav.addObject("searchValue3", searchVO.getSearchValue3());
		mav.addObject("list", list);
		mav.addObject("list2", totalAddress);
		return mav;
	}
	// 가게의 디테일 페이지 
	@GetMapping(value = "storeDetail")
	public ModelAndView mstoreDetail(int stnum, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("storeDetail");
		StoreVO vo = storeViewDao.storeDetail(stnum);
		mav.addObject("storevo", vo);
		return mav;
	}
}