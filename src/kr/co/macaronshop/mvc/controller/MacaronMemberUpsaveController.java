package kr.co.macaronshop.mvc.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.co.macaronshop.mvc.dto.MemberVO;
import kr.co.macaronshop.mvc.dto.PageVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;
import kr.co.macaronshop.mvc.dto.SearchVO;
import kr.co.macaronshop.mvc.dao.MyPageDao;
import kr.co.macaronshop.mvc.dao.PaymentDao;
import kr.co.macaronshop.mvc.dao.TboardDao;
import kr.co.macaronshop.mvc.dao.TboardDaoInter;
import kr.co.macaronshop.mvc.dto.TboardCommVO;
import kr.co.macaronshop.mvc.dto.TboardVO;



@Controller
public class MacaronMemberUpsaveController {
	private static final int BUFFER_SIZE = 4096;

	//TboardDao
	@Autowired
	private TboardDao tboardDao;
	
	//TboardDaoInter
	@Autowired
	private TboardDaoInter tboardDaoInter;
	
	
	@Autowired
	private PaymentDao paymentDao;
	
	// 후기 글 작성 폼 이동
	@RequestMapping(value = "/boardUpform")
	public ModelAndView upform(int pay_num) {
		 PaymentVO pvo = tboardDao.pamentDetail(pay_num);
		 ModelAndView mav = new ModelAndView();
			mav.setViewName("boardUpform");	
			mav.addObject("pvo",pvo);
		return mav;
	}
	
	// 게시글 저장 
	@RequestMapping(value = "/boardUpsave", method = RequestMethod.POST)
	public ModelAndView save(TboardVO vo, HttpSession session, HttpServletRequest request) {
        String img_path = "resources\\imgfile";
        String r_path = request.getRealPath("/");
		String oriFn = vo.getUpfile().getOriginalFilename();

		StringBuffer path = new StringBuffer();
		path.append(r_path).append(img_path).append("\\");
		path.append(oriFn);

		File f = new File(path.toString());
		try {
			vo.getUpfile().transferTo(f);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	
		vo.setImg(vo.getUpfile().getOriginalFilename());
		tboardDao.addTboard(vo);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/boardUplist?page=1");
		return mav;
	}

	// 게시글 전체 리스트 보기 
	@RequestMapping(value = "boardUplist")
	public ModelAndView mBoardlist(HttpServletRequest request,
		 int page,String searchType, String searchValue) {
		 PageVO pageInfo = new PageVO();
		  int rowsPerPage = 5; 
		  int pagesPerBlock = 5; 
		  int currentPage = page; 
		  int currentBlock = 0; 
		  if(currentPage % pagesPerBlock == 0){ 
		   currentBlock = currentPage / pagesPerBlock;
		  }else{
		   currentBlock = currentPage / pagesPerBlock + 1;
		  }
		  int startRow = (currentPage - 1) * rowsPerPage + 1;
		  int endRow = currentPage * rowsPerPage;
		  SearchVO svo = new SearchVO();
		  svo.setBegin(String.valueOf(startRow));
		  svo.setEnd(String.valueOf(endRow));
		  svo.setSearchType(searchType);
		  svo.setSearchValue(searchValue);
		  int totalRows = tboardDao.getTotalCount(svo);
		  int totalPages = 0;
		  if(totalRows % rowsPerPage == 0){
		   totalPages = totalRows / rowsPerPage;
		  }else{
		   totalPages = totalRows / rowsPerPage + 1;
		  }
		  int totalBlocks = 0;
		  if(totalPages % pagesPerBlock == 0){
		   totalBlocks = totalPages / pagesPerBlock;
		  }else{
		   totalBlocks = totalPages / pagesPerBlock + 1;
		  }
		  pageInfo.setCurrentPage(currentPage);
		  pageInfo.setCurrentBlock(currentBlock);
		  pageInfo.setRowsPerPage(rowsPerPage);
		  pageInfo.setPagesPerBlock(pagesPerBlock);
		  pageInfo.setStartRow(startRow);
		  pageInfo.setEndRow(endRow);
		  pageInfo.setTotalRows(totalRows);
		  pageInfo.setTotalPages(totalPages);
		  pageInfo.setTotalBlocks(totalBlocks);
		  ModelAndView mav = new ModelAndView();
		  mav.setViewName("boardUplist");
		  List<TboardVO> list = tboardDao.getListSearch(svo);
		  mav.addObject("pageInfo",pageInfo);
		  mav.addObject("searchType", searchType);
		  mav.addObject("searchValue", searchValue);
		  mav.addObject("list", list);
		  return mav;
	}
		// 특정 회원이 작성한 리뷰 리스트 
		@RequestMapping(value = "myReviewList")
		public ModelAndView reviewList(
			 int page,String searchType, String searchValue) {
			 PageVO pageInfo = new PageVO();
			  int rowsPerPage = 5; 
			  int pagesPerBlock = 5; 
			  int currentPage = page; 
			  int currentBlock = 0;  
			  if(currentPage % pagesPerBlock == 0){ 
			   currentBlock = currentPage / pagesPerBlock;
			  }else{
			   currentBlock = currentPage / pagesPerBlock + 1;
			  }
			  int startRow = (currentPage - 1) * rowsPerPage + 1;
			  int endRow = currentPage * rowsPerPage;
			  SearchVO svo = new SearchVO();
			  svo.setBegin(String.valueOf(startRow));
			  svo.setEnd(String.valueOf(endRow));
			  svo.setSearchType(searchType);
			  svo.setSearchValue(searchValue);
			  int totalRows = tboardDao.getTotalCount(svo);
			  int totalPages = 0;
			  if(totalRows % rowsPerPage == 0){
			   totalPages = totalRows / rowsPerPage;
			  }else{
			   totalPages = totalRows / rowsPerPage + 1;
			  }
			  int totalBlocks = 0;
			  if(totalPages % pagesPerBlock == 0){
			   totalBlocks = totalPages / pagesPerBlock;
			  }else{
			   totalBlocks = totalPages / pagesPerBlock + 1;
			  }
			  pageInfo.setCurrentPage(currentPage);
			  pageInfo.setCurrentBlock(currentBlock);
			  pageInfo.setRowsPerPage(rowsPerPage);
			  pageInfo.setPagesPerBlock(pagesPerBlock);
			  pageInfo.setStartRow(startRow);
			  pageInfo.setEndRow(endRow);
			  pageInfo.setTotalRows(totalRows);
			  pageInfo.setTotalPages(totalPages);
			  pageInfo.setTotalBlocks(totalBlocks);
			  ModelAndView mav = new ModelAndView();
			  mav.setViewName("myReviewList");
			  List<TboardVO> list = tboardDao.getListSearch(svo);
			  mav.addObject("pageInfo",pageInfo);
			  mav.addObject("searchType", searchType);
			  mav.addObject("searchValue", searchValue);
			  mav.addObject("list", list);
			  return mav;
		}
		
		// 리뷰 상세보기 
		@RequestMapping("/tboardDetail")
		public ModelAndView detailTboard(int postsnum) {
			ModelAndView mav = new ModelAndView("tboardDetail");
			
			tboardDao.tboardHitsUpdate(postsnum);
			
			TboardVO v = tboardDao.tboardDetail(postsnum); 
			List<TboardCommVO> cvlist = tboardDao.getList(postsnum);
			mav.addObject("v",v);
			mav.addObject("cvlist",cvlist);
			return mav;
		}
	
	// 리뷰 수정 폼 이동
	@GetMapping("/updateBorad")
	public ModelAndView surveyUpdateForm(HttpServletRequest req,int postsnum) {
		ModelAndView mav = new ModelAndView("updateBorad");
		TboardVO tvo = tboardDaoInter.tboardDetail(postsnum);
		req.setAttribute("tvo", tvo);
		mav.addObject("tvo", tvo);
		return mav;
	}
	
	// 리뷰 글 수정하기 
	@PostMapping("/tboardUpdate")
	public ModelAndView surveyUpdate(HttpServletRequest req,@ModelAttribute("tvo") TboardVO tvo,HttpSession session) 
	{

		String path = "/resources/imgfile/";
		String paths = req.getRealPath("/");
		
		if(!tvo.getUpfile().getOriginalFilename().equals("")) {
			tvo.setImg(tvo.getUpfile().getOriginalFilename());
			StringBuffer path1 = new StringBuffer();
			path1.append(paths).append(path).append("\\");
			path1.append(tvo.getImg());
		File f = new File(path1.toString());
		try {
			tvo.getUpfile().transferTo(f);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		}
		
		tboardDao.tboardUpdate(tvo);
		
		ModelAndView mav = new ModelAndView("redirect:/boardUplist?page=1");
		return mav;
	}
	
	// 리뷰 글 삭제 
	@GetMapping("/tboardDelete")
	public ModelAndView surveyDelite(HttpServletRequest req, int postsnum) {
		tboardDaoInter.tboardDelete(postsnum);
		ModelAndView mav = new ModelAndView("redirect:/boardUplist?page=1");
		return mav;
	}

	// 특정 회원의 주문 내역 리스트 
	@RequestMapping("orderList")
	public ModelAndView orderList(HttpSession session) {
		ModelAndView mav = new ModelAndView("orderList");
		String memid = (String) session.getAttribute("uid");
		List<PaymentVO> pay_list = paymentDao.getpaylist(memid);
		mav.addObject("pay_list", pay_list);
		return mav;
	}
	
	// 댓글 작성 폼
	@PostMapping("/tboardAddComm")
	public ModelAndView addTboardcomm(TboardCommVO v) {
		tboardDao.addTboardComm(v);
		ModelAndView mav = new ModelAndView("redirect:tboardDetail?postsnum="+v.getTnum());
		return mav;
	}
	
	// 댓글 수정하기 폼 
	@GetMapping("/commUpBoradform") 
	public String commUpBoradform(HttpServletRequest req,int postsnum) {
		TboardCommVO tvo = tboardDaoInter.commDetail(postsnum);
		req.setAttribute("tvo", tvo);
		return "commUpBoradform";
	}
	
	// 댓글 수정 
	@PostMapping("/commboardUpdate")
	public String commUpdate(HttpServletRequest req,TboardCommVO tvo,TboardVO vo, int tnum) 
	{ 

		tboardDaoInter.commupdate(tvo);
		return "redirect:tboardDetail?postsnum="+tvo.getTnum();
	}

	// 댓글 삭제
	@GetMapping("/commDelete")
	public ModelAndView commDelete(HttpServletRequest req, int postsnum, int postsnum2) {
		tboardDaoInter.commDelete(postsnum);
		ModelAndView mav = new ModelAndView("redirect:tboardDetail?postsnum="+postsnum2);
		return mav;
	}
	

}
