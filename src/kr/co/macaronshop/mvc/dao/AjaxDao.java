package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.Mem_Interest_LogVO;
import kr.co.macaronshop.mvc.dto.StoreVO;
import kr.co.macaronshop.mvc.dto.TboardVO;

@Repository
public class AjaxDao {
	@Autowired
	private SqlSessionTemplate ss;
	// ajax 에서 데이터 처리를 위한 dao
	
	// 가게의 메뉴를 출력하여 주기 위한 메서드
	public List<StoreVO> store_menu(int stnum) {
		return ss.selectList("storeMenu.menuList", stnum);
	}
	
	// 특정 회원의 관심도 출력을 위한 메서드 
	public List<Mem_Interest_LogVO> memInterLog(int unum){
		return ss.selectList("meminter.memInter", unum);
	}
	
	// 평점 높은 가게를 출력 하기 위한 메서드 
	public List<TboardVO> popularStore(){
		return ss.selectList("meminter.parameter");
	}
}
