package kr.co.macaronshop.mvc.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.Mem_Interest_LogVO;

@Repository
public class AopInterestDao {
	@Autowired
	private SqlSessionTemplate ss;
	
	// 회원 관심도 로그 체크를 위한 메서드 
	public Mem_Interest_LogVO memInterestLog(Mem_Interest_LogVO vo) {
		return ss.selectOne("meminter.searchLog", vo);
	}
	
	// 처음 가게의 관심을 보인 회원의 로그 기록을 등록하기 위한 메서드 
	public void firstMemIntLog(Mem_Interest_LogVO vo) {
		ss.insert("meminter.firstMemIntLog", vo);
	}

	// 관심을 보였던 가게에 또 한번 구매시 update 처리를 위한 메서드 
	public void updateMemIntLog(Mem_Interest_LogVO vo) {
		ss.update("meminter.updateMemIntLog",vo);
	}

}
