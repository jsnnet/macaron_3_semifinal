package kr.co.macaronshop.mvc.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.BasketListDTO;
import kr.co.macaronshop.mvc.dto.BasketVO;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Repository
public class BasketDao {
	
	@Autowired
	private SqlSessionTemplate ss;
	
		// 장바구니 리스트 출력을 위한 메서드 
		public List<BasketListDTO> listBasket2(int memnum){
			return ss.selectList("cart.listBasket2", memnum);
		} 
	
		// 장바구니 등록 
		public void intoCart(BasketVO vo) {
			ss.insert("cart.addBasket", vo);
		}
	
		// 장바구니에 등록된 제품들의 총가격
		public BasketListDTO totalPrice(int memnum) {
			return ss.selectOne("cart.totalPrice", memnum);
		}
		
		// 장바구니 담긴 제품 단일 삭제 
		public void delCart(int basketnum) {
			ss.delete("cart.delCart", basketnum);
		}

		// 장바구니 결제 
		public BasketVO cartPay(int basketnum) {
			return ss.selectOne("cart.cartpay", basketnum);
		}

		// 결제 테이블에 추가 
		public void insertPay(BasketVO pvo) {
			ss.insert("cart.insertpay", pvo);
		}
}
