package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.LoginLoggerDTO;
import kr.co.macaronshop.mvc.dto.MemberVO;
@Repository
public class MemberDao{
	
	@Autowired
	private SqlSessionTemplate ss;
	
	// 회원 가입 
	public void addMem(MemberVO vo) {
		ss.insert("member.join",vo);
	}
	// 아이디 중복 체크 
	public int idchk(String id) {
		return ss.selectOne("member.idchk",id);
	}
	// 로그인 체크 
	public MemberVO loginCheck(MemberVO vo) {
		return ss.selectOne("member.loginchk",vo);
	}
	// 회원의 로그인 기록 로그 
	public void addLoginLogging(LoginLoggerDTO vo) {
		ss.insert("member.logger_in",vo);
		
	}
	public List<LoginLoggerDTO> mypage(String id) {
		return ss.selectList("member.mypage",id);
	}
	
	// 회원 정보 수정 
	public void update(MemberVO vo) {
		ss.update("member.update",vo);
	}
	
	// 로그를 리스트로 출력 
	public List<LoginLoggerDTO> mylog(String id) {
		return ss.selectList("member.mylog",id);
	}

}
