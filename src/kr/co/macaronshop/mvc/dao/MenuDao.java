package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.MenuVO;
import kr.co.macaronshop.mvc.dto.StoreVO;

@Repository
public class MenuDao {

	@Autowired
	private SqlSessionTemplate ss;
	
	// 가게 메뉴 등록 
	public void menuInsert(MenuVO vo) {
		ss.insert("menu.ins", vo);
	}

	// 메뉴 리스트 
	public List<StoreVO> getlist(int stnum){
		return ss.selectList("mypage.menulist",stnum);
	}
	
	// 메뉴 수정시 필요한 메뉴의 정보를 뽑아내는 메서드
	public MenuVO getMenu(int foodnum) {
		return ss.selectOne("menu.menu1",foodnum);
	}

	// 메뉴 수정 
	public void getmenuupdate(MenuVO mvo) {
		ss.update("menu.update",mvo);
	}
	
	// 메뉴 삭제 
	public void getdelet(int foodnum) {
	      ss.delete("menu.menudelet",foodnum);
	}
}
