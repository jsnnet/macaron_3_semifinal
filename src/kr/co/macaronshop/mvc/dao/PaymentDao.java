package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.Dutch_PayVO;
import kr.co.macaronshop.mvc.dto.PaymentVO;

@Repository
public class PaymentDao {
	@Autowired
	private SqlSessionTemplate ss;

	public void getInsert(PaymentVO pvo) {
		ss.insert("pay_system.pay", pvo);
	}
	//구매 이력 확인
	public List<PaymentVO> getpaylist(String memid) {
		return ss.selectList("pay_system.paylist", memid);
	}
	//친구 있는지 확인
	public int getCountFriend(String memid) {
		return ss.selectOne("pay_system.friendChk",memid);
	}
	
	//진행중인 더치페이 유무 확인
	//결제대기 유무 확인
	public int getProStatus(int memnum) {
		return ss.selectOne("pay_system.proStatus", memnum);
	}
	//PayNum 결제번호 출력
	public int getPayNum(int memnum) {
		return ss.selectOne("pay_system.payNum",memnum);
	}
	
	//결제대기상태로 결제 테이블에 들어감
	public void setPaymentDutchInsert(PaymentVO pvo) {
		ss.insert("pay_system.paymentdutchpay", pvo);
	}
	
	//더치페이 
	public void setDutchInsert(Dutch_PayVO dvo) {
		ss.insert("pay_system.dutchpay", dvo);
	}
	
	//setDutchUpdate 결제 후 값 수정
	public void setDutchUpdate(Dutch_PayVO dvo) {
	
		ss.update("pay_system.dutchpayUpdate",dvo);
	}
	
	//setdutch_delete 결제 취소
	public void setdutch_delete(int pay_num) {
		ss.delete("pay_system.dutch_delete",pay_num);
		ss.delete("pay_system.payment_delete",pay_num);
	}
	//setUpdatepay -> 최종 결제
	public void setUpdatepay(int pay_num) {
		ss.update("pay_system.updatepay",pay_num);
	}
}
