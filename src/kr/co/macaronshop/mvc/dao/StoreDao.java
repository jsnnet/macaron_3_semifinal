package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.PaymentVO;
import kr.co.macaronshop.mvc.dto.StoreVO;
@Repository
public class StoreDao{
	@Autowired
	private SqlSessionTemplate ss;
	
	// 가게 등록
   public void addStore(StoreVO vo) {
	   ss.insert("store.join",vo);
   }

   // 아이디 중복 검사 
   public int idChk(String stid) {
	   return ss.selectOne("store.idchk",stid);
   }

   // 로그인 체크 
   public StoreVO loginCheck(StoreVO vo) {
	   return ss.selectOne("store.loginchk",vo);
   }
	
   // 가게 마이페이지에서 가게 주소를 출력
	public StoreVO getStoreinfor(int stnum) {
		return ss.selectOne("store.storeinfor",stnum);
	}
	
	// 특정 가게에 주문 내역 리스트 
	public List<PaymentVO> getOrderList(int stnum){
		return ss.selectList("store.orderList",stnum); 
	}
	
	// 가게 정보 수정 
	public void update(StoreVO vo) {
		ss.update("store.stmyupdate",vo);
	}
}
