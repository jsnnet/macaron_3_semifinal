package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.SearchVO;
import kr.co.macaronshop.mvc.dto.StoreVO;



@Repository
public class StoreViewDao {
	@Autowired
	private SqlSessionTemplate ss;
	
	// 가게 리스트 뽑기
	public List<StoreVO> getList(SearchVO svo){
		return ss.selectList("showstore.list", svo);
	}
	// 검색된 전체 갯수
	public int getTotalCount(SearchVO svo) {
		return ss.selectOne("showstore.totalCount", svo);
	}
	// 검색된 가게 리스트 
	public List<StoreVO> getListSearch(SearchVO svo){
		return ss.selectList("showstore.listsearch", svo);
	}
	// 특정 가게의 디테일 정보
	public StoreVO storeDetail(int stnum) {
		return ss.selectOne("showstore.storeDetail", stnum);
	}
	// 카카오 api를 사용하기위한 모든 가게의 주소를 출력 
	public List<StoreVO> getTotalAddress() {
		// TODO Auto-generated method stub
		return ss.selectList("showstore.totaladdress");
	}
}
