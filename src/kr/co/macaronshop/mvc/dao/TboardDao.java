package kr.co.macaronshop.mvc.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.macaronshop.mvc.dto.PaymentVO;
import kr.co.macaronshop.mvc.dto.SearchVO;
import kr.co.macaronshop.mvc.dto.TboardCommVO;
import kr.co.macaronshop.mvc.dto.TboardVO;


@Repository
public class TboardDao implements TboardDaoInter {
	@Autowired
	private SqlSessionTemplate ss;
	
	/**게시판**/
	/*게시판 - 리스트 및 수정,삭제,넣기 기능*/
	public void addTboard(TboardVO v) {
		ss.insert("tboard.ins",v);
	}
	
	//내가 작성한 리뷰 리스트 전체보기
	public List<TboardVO> myListTboard(String memid) {
		return ss.selectList("tboard.myListAll",memid);
	}
	
	@Override //판매테이블 주문자,주문번호,가게번호
	public PaymentVO pamentDetail(int pay_num) {
		return ss.selectOne("tboard.store",pay_num);
	}
	
	//게시물 리스트로 보기
	public List<TboardVO> listTboard(int postsnum){
		return ss.selectList("tboard.listAll",postsnum);
	}

	//게시글 수정하기
	@Override
	public void tboardUpdate(TboardVO tvo) {
		ss.update("tboard.tboardUpdate",tvo);
	}
	
	//게시물 조회수
	public void tboardHitsUpdate(int postsnum){
	ss.update("tboard.tboardHitsUpdate",postsnum);
	}
	
	//게시물 상세보기
	@Override
	public TboardVO tboardDetail(int postsnum) {
		return ss.selectOne("tboard.tboardDetail",postsnum);
	}
	
	//게시물 지우기, 본인의 글만 삭제 가능하도록 해야 함.
	@Override
	public void tboardDelete(int postsnum) {
	ss.delete("tboard.tboardDelete",postsnum);
	}

	
	/* 댓글 추가하기 및 댓글 페이지! */
	//상세보기에 해당되는 사용자의 댓글 리스트
	public List<TboardCommVO> getList(int tnum){
		return ss.selectList("tboard.commList",tnum);
	}
	
	/*//게시물 리스트로 보기
	public List<TboardVO> listTboard(int postsnum){
		return ss.selectList("tboard.listAll",postsnum);
	}*/
	
	//전체 댓글 리스트, postsnum으로 조회
	@Override
	public List<TboardCommVO> commList() {
	return ss.selectList("tboard.commList2");
	}
	
	//댓글 입력
	public void addTboardComm(TboardCommVO v) {
			ss.insert("tboard.inscomm",v);
	}
	
	//댓글 글 하나 상세보기
	@Override
	public TboardCommVO commDetail(int postsnum) {
		return ss.selectOne("tboard.commDetail", postsnum);
	}
	
	//댓글 수정하기
	@Override
	public void commupdate(TboardCommVO tvocomm) {
		ss.update("tboard.commupdate",tvocomm);
		
	}
	
	//댓글 삭제하기
	@Override
	public void commDelete(int postsnum) {
		ss.delete("tboard.commDelete",postsnum);
	}
	
	//검색
	public List<TboardVO> getListSearch(SearchVO svo){
		return ss.selectList("tboard.listsearch",svo);
	}
	
	public int getTotalCount(SearchVO tvo) {
		return ss.selectOne("tboard.totalCount");
	}
    
	public List<TboardVO> gradeList(){
		return ss.selectList("tboard.gradeList");
	}
	
}
