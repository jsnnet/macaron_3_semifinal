package kr.co.macaronshop.mvc.dao;

import java.util.List;

import kr.co.macaronshop.mvc.dto.PaymentVO;
import kr.co.macaronshop.mvc.dto.TboardCommVO;
import kr.co.macaronshop.mvc.dto.TboardVO;



public interface TboardDaoInter {
public List<TboardVO> listTboard(int postsnum);
public List<TboardVO> myListTboard(String memid);
public void tboardUpdate(TboardVO tvo);
public void tboardDelete(int postsnum);
public TboardVO tboardDetail(int postsnum);//게시물 상세보기

public PaymentVO pamentDetail(int memnum);//판매테이블 주문자,주문번호,가게번호 

//댓글
public void commupdate(TboardCommVO tvocomm);
public void commDelete(int postsnum);
public TboardCommVO commDetail(int postsnum);//댓글 상세보기
public List<TboardCommVO> commList();//댓글 전체 리스트
}
