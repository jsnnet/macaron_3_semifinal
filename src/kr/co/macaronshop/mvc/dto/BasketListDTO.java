package kr.co.macaronshop.mvc.dto;

import org.springframework.web.multipart.MultipartFile;

public class BasketListDTO { 
	private String foodname, foodimg, fooddetail, category;
	private int foodnum, foodtotal, foodpay, basketpay, basketnum, memnum, stnum, totalprice, dok;
	private MultipartFile showimg;
	public String getFoodname() {
		return foodname;
	}
	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}
	public String getFoodimg() {
		return foodimg;
	}
	public void setFoodimg(String foodimg) {
		this.foodimg = foodimg;
	}
	public String getFooddetail() {
		return fooddetail;
	}
	public void setFooddetail(String fooddetail) {
		this.fooddetail = fooddetail;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getFoodnum() {
		return foodnum;
	}
	public void setFoodnum(int foodnum) {
		this.foodnum = foodnum;
	}
	public int getFoodtotal() {
		return foodtotal;
	}
	public void setFoodtotal(int foodtotal) {
		this.foodtotal = foodtotal;
	}
	public int getFoodpay() {
		return foodpay;
	}
	public void setFoodpay(int foodpay) {
		this.foodpay = foodpay;
	}
	public int getBasketpay() {
		return basketpay;
	}
	public void setBasketpay(int basketpay) {
		this.basketpay = basketpay;
	}
	public int getBasketnum() {
		return basketnum;
	}
	public void setBasketnum(int basketnum) {
		this.basketnum = basketnum;
	}
	public int getMemnum() {
		return memnum;
	}
	public void setMemnum(int memnum) {
		this.memnum = memnum;
	}
	public int getStnum() {
		return stnum;
	}
	public void setStnum(int stnum) {
		this.stnum = stnum;
	}
	public int getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(int totalprice) {
		this.totalprice = totalprice;
	}
	public int getDok() {
		return dok;
	}
	public void setDok(int dok) {
		this.dok = dok;
	}
	public MultipartFile getShowimg() {
		return showimg;
	}
	public void setShowimg(MultipartFile showimg) {
		this.showimg = showimg;
	}
	
	
}
