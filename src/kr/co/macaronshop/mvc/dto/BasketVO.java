package kr.co.macaronshop.mvc.dto;

public class BasketVO {
	
	private int basketnum, memnum, stnum, foodnum, basketpay, foodtotal, dok;
	private String basket_time,foodname,category;
	public int getBasketnum() {
		return basketnum;
	}
	public void setBasketnum(int basketnum) {
		this.basketnum = basketnum;
	}
	public int getMemnum() {
		return memnum;
	}
	public void setMemnum(int memnum) {
		this.memnum = memnum;
	}
	public int getStnum() {
		return stnum;
	}
	public void setStnum(int stnum) {
		this.stnum = stnum;
	}
	public int getFoodnum() {
		return foodnum;
	}
	public void setFoodnum(int foodnum) {
		this.foodnum = foodnum;
	}
	public int getBasketpay() {
		return basketpay;
	}
	public void setBasketpay(int basketpay) {
		this.basketpay = basketpay;
	}
	public int getFoodtotal() {
		return foodtotal;
	}
	public void setFoodtotal(int foodtotal) {
		this.foodtotal = foodtotal;
	}
	public int getDok() {
		return dok;
	}
	public void setDok(int dok) {
		this.dok = dok;
	}
	public String getBasket_time() {
		return basket_time;
	}
	public void setBasket_time(String basket_time) {
		this.basket_time = basket_time;
	}
	public String getFoodname() {
		return foodname;
	}
	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
