package kr.co.macaronshop.mvc.dto;

import java.util.List;

public class Dutch_PayVO {
	
	private int pay_num,pay_total,pay1,pay2,pay3,maxpay,pay4;
	
	private String memid1,memid2,memid3,memid,pay;
	
	public int getPay4() {
		return pay4;
	}
	public void setPay4(int pay4) {
		this.pay4 = pay4;
	}
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}
	public String getMemid() {
		return memid;
	}
	public void setMemid(String memid) {
		this.memid = memid;
	}
	private PaymentVO pvo;

	public int getMaxpay() {
		return maxpay;
	}
	public void setMaxpay(int maxpay) {
		this.maxpay = maxpay;
	}


	public PaymentVO getPvo() {
		return pvo;
	}
	public void setPvo(PaymentVO pvo) {
		this.pvo = pvo;
	}
	public int getPay_num() {
		return pay_num;
	}
	public void setPay_num(int pay_num) {
		this.pay_num = pay_num;
	}
	public int getPay_total() {
		return pay_total;
	}
	public void setPay_total(int pay_total) {
		this.pay_total = pay_total;
	}
	public int getPay1() {
		return pay1;
	}
	public void setPay1(int pay1) {
		this.pay1 = pay1;
	}
	public int getPay2() {
		return pay2;
	}
	public void setPay2(int pay2) {
		this.pay2 = pay2;
	}
	public int getPay3() {
		return pay3;
	}
	public void setPay3(int pay3) {
		this.pay3 = pay3;
	}
	public String getMemid1() {
		return memid1;
	}
	public void setMemid1(String memid1) {
		this.memid1 = memid1;
	}
	public String getMemid2() {
		return memid2;
	}
	public void setMemid2(String memid2) {
		this.memid2 = memid2;
	}
	public String getMemid3() {
		return memid3;
	}
	public void setMemid3(String memid3) {
		this.memid3 = memid3;
	}
	
	

}
