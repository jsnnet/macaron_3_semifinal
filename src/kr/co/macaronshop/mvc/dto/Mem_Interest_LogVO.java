package kr.co.macaronshop.mvc.dto;

public class Mem_Interest_LogVO {

	private int mem_log_num, memnum, stnum, foodnum, stcnt, foodcnt;
	private String log_time;
	
	public int getStcnt() {
		return stcnt;
	}
	public void setStcnt(int stcnt) {
		this.stcnt = stcnt;
	}
	public int getFoodcnt() {
		return foodcnt;
	}
	public void setFoodcnt(int foodcnt) {
		this.foodcnt = foodcnt;
	}
	public int getMem_log_num() {
		return mem_log_num;
	}
	public void setMem_log_num(int mem_log_num) {
		this.mem_log_num = mem_log_num;
	}
	public int getMemnum() {
		return memnum;
	}
	public void setMemnum(int memnum) {
		this.memnum = memnum;
	}
	public int getStnum() {
		return stnum;
	}
	public void setStnum(int stnum) {
		this.stnum = stnum;
	}
	public int getFoodnum() {
		return foodnum;
	}
	public void setFoodnum(int foodnum) {
		this.foodnum = foodnum;
	}
	public String getLog_time() {
		return log_time;
	}
	public void setLog_time(String log_time) {
		this.log_time = log_time;
	}
}
