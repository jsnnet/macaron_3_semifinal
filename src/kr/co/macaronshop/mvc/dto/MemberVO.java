package kr.co.macaronshop.mvc.dto;

public class MemberVO {
	
	private int memnum,memage,mempoint ,cnt;
	private String memid,mempwd,mememail,memname,memgender,memphone,mempost,memaddr1,memaddr2;
	
	public String getMempost() {
		return mempost;
	}
	public void setMempost(String mempost) {
		this.mempost = mempost;
	}
	public String getMemaddr1() {
		return memaddr1;
	}
	public void setMemaddr1(String memaddr1) {
		this.memaddr1 = memaddr1;
	}
	public String getMemaddr2() {
		return memaddr2;
	}
	public void setMemaddr2(String memaddr2) {
		this.memaddr2 = memaddr2;
	}
	public String getMememail() {
		return mememail;
	}
	public void setMememail(String mememail) {
		this.mememail = mememail;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public int getMemnum() {
		return memnum;
	}
	public void setMemnum(int memnum) {
		this.memnum = memnum;
	}
	public int getMemage() {
		return memage;
	}
	public void setMemage(int memage) {
		this.memage = memage;
	}
	public int getMempoint() {
		return mempoint;
	}
	public void setMempoint(int mempoint) {
		this.mempoint = mempoint;
	}
	public String getMemid() {
		return memid;
	}
	public void setMemid(String memid) {
		this.memid = memid;
	}
	public String getMempwd() {
		return mempwd;
	}
	public void setMempwd(String mempwd) {
		this.mempwd = mempwd;
	}
	public String getMemname() {
		return memname;
	}
	public void setMemname(String memname) {
		this.memname = memname;
	}
	public String getMemgender() {
		return memgender;
	}
	public void setMemgender(String memgender) {
		this.memgender = memgender;
	}
	public String getMemphone() {
		return memphone;
	}
	public void setMemphone(String memphone) {
		this.memphone = memphone;
	}

}
