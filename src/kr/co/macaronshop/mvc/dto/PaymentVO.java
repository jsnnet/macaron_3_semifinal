
package kr.co.macaronshop.mvc.dto;

public class PaymentVO {

private int pay_num, memnum, stnum, foodtotal, dok, foodpay, pay_total, dutch_ok, pro_status;
private String pay_time, foodname, foodimg, fooddetail, category;
private MenuVO menu;
private StoreVO store;
private MemberVO membervo;
public int getPay_num() {
	return pay_num;
}
public void setPay_num(int pay_num) {
	this.pay_num = pay_num;
}
public int getMemnum() {
	return memnum;
}
public void setMemnum(int memnum) {
	this.memnum = memnum;
}
public int getStnum() {
	return stnum;
}
public void setStnum(int stnum) {
	this.stnum = stnum;
}
public int getFoodtotal() {
	return foodtotal;
}
public void setFoodtotal(int foodtotal) {
	this.foodtotal = foodtotal;
}
public int getDok() {
	return dok;
}
public void setDok(int dok) {
	this.dok = dok;
}
public int getFoodpay() {
	return foodpay;
}
public void setFoodpay(int foodpay) {
	this.foodpay = foodpay;
}
public int getPay_total() {
	return pay_total;
}
public void setPay_total(int pay_total) {
	this.pay_total = pay_total;
}
public int getDutch_ok() {
	return dutch_ok;
}
public void setDutch_ok(int dutch_ok) {
	this.dutch_ok = dutch_ok;
}
public int getPro_status() {
	return pro_status;
}
public void setPro_status(int pro_status) {
	this.pro_status = pro_status;
}
public String getPay_time() {
	return pay_time;
}
public void setPay_time(String pay_time) {
	this.pay_time = pay_time;
}
public String getFoodname() {
	return foodname;
}
public void setFoodname(String foodname) {
	this.foodname = foodname;
}
public String getFoodimg() {
	return foodimg;
}
public void setFoodimg(String foodimg) {
	this.foodimg = foodimg;
}
public String getFooddetail() {
	return fooddetail;
}
public void setFooddetail(String fooddetail) {
	this.fooddetail = fooddetail;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public MenuVO getMenu() {
	return menu;
}
public void setMenu(MenuVO menu) {
	this.menu = menu;
}
public StoreVO getStore() {
	return store;
}
public void setStore(StoreVO store) {
	this.store = store;
}
public MemberVO getMembervo() {
	return membervo;
}
public void setMembervo(MemberVO membervo) {
	this.membervo = membervo;
}


}
