package kr.co.macaronshop.mvc.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class StoreVO {
	private int stnum;
	private String stsbname, stname, stid, stpwd, stpost,staddr1,staddr2, stphone, stimg, stemail;

	
	private List<MenuVO> menu;
	private List<Mem_Interest_LogVO> mem_interest_log;
	private List<TboardVO> boardvo;
	
	private MultipartFile multipartFile;
	private String path;
	
	public List<TboardVO> getBoardvo() {
		return boardvo;
	}
	public void setBoardvo(List<TboardVO> boardvo) {
		this.boardvo = boardvo;
	}
	public List<Mem_Interest_LogVO> getMem_interest_log() {
		return mem_interest_log;
	}
	public void setMem_interest_log(List<Mem_Interest_LogVO> mem_interest_log) {
		this.mem_interest_log = mem_interest_log;
	}
	public String getStemail() {
		return stemail;
	}
	public void setStemail(String stemail) {
		this.stemail = stemail;
	}
	public String getStpost() {
		return stpost;
	}
	public void setStpost(String stpost) {
		this.stpost = stpost;
	}
	public String getStaddr1() {
		return staddr1;
	}
	public void setStaddr1(String staddr1) {
		this.staddr1 = staddr1;
	}
	public String getStaddr2() {
		return staddr2;
	}
	public void setStaddr2(String staddr2) {
		this.staddr2 = staddr2;
	}
	
	public List<MenuVO> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuVO> menu) {
		this.menu = menu;
	}
	public int getStnum() {
		return stnum;
	}
	public void setStnum(int stnum) {
		this.stnum = stnum;
	}
	public String getStsbname() {
		return stsbname;
	}
	public void setStsbname(String stsbname) {
		this.stsbname = stsbname;
	}
	public String getStname() {
		return stname;
	}
	public void setStname(String stname) {
		this.stname = stname;
	}
	public String getStid() {
		return stid;
	}
	public void setStid(String stid) {
		this.stid = stid;
	}
	public String getStpwd() {
		return stpwd;
	}
	public void setStpwd(String stpwd) {
		this.stpwd = stpwd;
	}
	public String getStphone() {
		return stphone;
	}
	public void setStphone(String stphone) {
		this.stphone = stphone;
	}
	public String getStimg() {
		return stimg;
	}
	public void setStimg(String stimg) {
		this.stimg = stimg;
	}
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
