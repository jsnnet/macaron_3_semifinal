package kr.co.macaronshop.mvc.dto;

public class TboardCommVO {
	
	private int postsnum;
	private int tnum;
	private String tdate; 
	private String comments;
	private String memid;
	
	public int getPostsnum() {
		return postsnum;
	}
	public void setPostsnum(int postsnum) {
		this.postsnum = postsnum;
	}
	public int getTnum() {
		return tnum;
	}
	public void setTnum(int tnum) {
		this.tnum = tnum;
	}
	public String getTdate() {
		return tdate;
	}
	public void setTdate(String tdate) {
		this.tdate = tdate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getMemid() {
		return memid;
	}
	public void setMemid(String memid) {
		this.memid = memid;
	}
	

	
}
