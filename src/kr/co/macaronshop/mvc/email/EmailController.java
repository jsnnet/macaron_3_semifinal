package kr.co.macaronshop.mvc.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.co.macaronshop.mvc.dto.EmailVO;

@Controller
public class EmailController {

	@Autowired
	private EmailSender emailSender;

	// 메일 발송
	@RequestMapping(value = "send", method = RequestMethod.POST)
	public String submit(EmailVO form) throws Exception {
		emailSender.sendEmail(form);
		return "success";
	}

	// 메일 발송 폼 이동
	@RequestMapping("/mailform")
	public String mailform() {
		return "mailform";
	}
}
