package kr.co.macaronshop.mvc.email;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.co.macaronshop.mvc.dto.EmailVO;

import org.springframework.mail.javamail.JavaMailSender;

@Component
public class EmailSender {
    @Autowired
    private JavaMailSender mailSender;
    
    // 메일 발송 
    public void sendEmail(EmailVO emailFomr) throws Exception{
    	String name = "이름 : " + emailFomr.getName() + "\n";
        MimeMessage msg = mailSender.createMimeMessage();
        msg.setSubject(emailFomr.getSubject()); //메일 제공
        msg.setText(name + emailFomr.getContent());
        msg.setRecipient(RecipientType.TO, new InternetAddress(emailFomr.getReceiver()));
        mailSender.send(msg);
    }
}


